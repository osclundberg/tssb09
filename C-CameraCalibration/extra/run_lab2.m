clear Xplane;
clear XImg;


imageIndex = 1;

while 1
  answer = input('\nPrepare for next image... press a key when ready, or q to quit', 's');
  if isequal(answer, 'q')
    break;
  end
  
  fprintf('\nProcessing...');
  
%  img = imread('http://cvl-cam-00/jpg/hugesize.jpg');
  img = imread('http://cvl-cam-00.edu.isy.liu.se/jpg/image.jpg');
  img = rgb2gray(im2double(img));
  
  [P_img, P_world] = detect_L_pattern(img, [], 1);
  
  fprintf('done!\nInspect the results (press a key when done)');
  pause;
  answer = input('\nKeep this one (y/n)?', 's');
  
  if isequal(answer, 'y')
    % Yes, keep these points. Include them in the Xplane and 
    % XImg cell arrays
    Xplane{imageIndex} = P_world;
    XImg{imageIndex}   = P_img;
    fprintf('Number of kept planes = %d\n', imageIndex);
    imageIndex = imageIndex + 1;
  end

end


% Run Zhang's calibration method
[A, R, t] = CameraCalibration(Xplane, XImg,1)

