%function [origin,xdirn]=extract_origin(X,Y,V)
%
% Extract origin from cluster of 4 of
% the 7 strongest corner points in
% the corner image IM. 
%
% X,Y  Coordinate point list
% V    Corresponding point strengths
%
%Per-Erik Forssen, March 2003

function [origin,xdirn,ydirn]=extract_origin(X,Y,V)

[Vs,ind7]=sort(V);
ind7=ind7(end-6:end); % Keep 7 largest
X=X(ind7);
Y=Y(ind7);

% Remove 3 outliers
for k=1:3,
  Xm=mean(X);
  Ym=mean(Y);
  R=sqrt((X-Xm).^2+(Y-Ym).^2);
  [mv,mind]=max(R);
  
  if(mv<100),
    % Exclude points based on magnitude if all points are close
    X=X(2:end);  
    Y=Y(2:end);
  else
    % Exclude points based on distance otherwise
    keep_list=find([1:size(X,1)]~=mind);
    X=X(keep_list);
    Y=Y(keep_list);
  end;
end

%  Find corner of L by looking at distance to cluster
Xm=mean(X);
Ym=mean(Y);
R=sqrt((X-Xm).^2+(Y-Ym).^2);
[mv,mind]=sort(R);

% Temp Origin (corner of L)
Xo=X(mind(2));
Yo=Y(mind(2));

% Vector to point to the right
xdirn=[X(mind(1))-Xo;Y(mind(1))-Yo];

% Vector to point below
ydirn=[X(mind(3))-Xo;Y(mind(3))-Yo];

% Approx origin
origin=[Xo;Yo]-xdirn/2-ydirn/2;

% Find right-vector with longer baseline
Xm=Xo+2*xdirn(1);
Ym=Yo+2*xdirn(2);
R=sqrt((X-Xm).^2+(Y-Ym).^2);
[mv,mind]=min(R);
xdirn=[X(mind)-Xo;Y(mind)-Yo]/2;

% Normalise vectors
%xdirn=xdirn/sqrt(xdirn'*xdirn);
%ydirn=ydirn/sqrt(ydirn'*ydirn);