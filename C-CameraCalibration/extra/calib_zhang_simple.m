function  [A, R, t] = calib_zhang_simple(Xplane, Ximg)
%
%  [A, R, t] = calib_zhang_simple(Xplane, Ximg)
%
%  Calibrate camera according to [Zhang, 1998]
%  (research.microsoft.com/~zhang), but without the final iterative
%  improvement step (hence the word "simple" in the filename. Uses
%  detected poins on a 2D calibration pattern, seen from different views.
%  The true 3D position of the points are not needed - only 2D positions
%  on the calibration plane. The method returns one intrinsic matrix and
%  extrinsic parameters for each view.
%
%  Parameters:
%    Xplane:  Cell array of point lists. Xplane{ii} is an 
%             Nx2 array of coordinates on the calibration pattern
%             plane for the detected image points in image ii.
%             
%    Ximg:    Cell array of point lists. Ximg{ii} is an
%             Nx2 array of image coordinates for the detected
%             points on the calibration pattern from image ii.
%
%  Return values:
%    A:       Intrinsic parameters
%    R:       Cell array of rotation matrices for each view
%    t:       Cell array of translation vectors for each view
%
%  Places the homogeneous coordinate at the end (i.e. [x y 1])
%
%  Note: There is an implementation of the entire Zhang calibrator
%  by Zhang himself as a stand-alone windows executable working on
%  text files available from his homepage.
%

%  Note:
%  This file is based on code from Maria, Per-Erik and some student
%  projekt, and is a restructured version of the code used in the
%  Camera Calibration lab in the "Bildanalys" course. I made it
%  into a proper function, added support for an arbitrary number
%  of views, and called homography_stls instead of some non-normalized
%  version from the student project. /ErikJ, 2006

% Generate homographies for each view
for ii = 1:length(Xplane)
  xp = Xplane{ii};
  xi = Ximg{ii};
  
  Hbig(:,:,ii) = homography_stls(xp, xi);
end

% Generate the intrinsic parameters from the homographies
A = homography2intrinsic(Hbig);
invA = inv(A);

% Compute extrinsic parameters for each view
for ii = 1:length(Xplane)
  H = Hbig(:,:,ii);
  H = H / H(end);
  % Note: The resulting rotation matrix has a mirror-ambiguity. Here, we
  % make sure H(end)=1, in order to get the exact same result as the old
  % Bildanalys-lab version, that uses a different homography estimator.
  % In the future, this normalization could be removed.
  
  h1 = H(:,1);
  h2 = H(:,2);
  h3 = H(:,3);
  lambda=1/norm(invA*h1);
  
  r1=lambda*invA*h1;
  r2=lambda*invA*h2;
  r3=cross(r1,r2);
  t{ii} = lambda*invA*h3;
  R{ii} = [r1,r2,r3];
end

