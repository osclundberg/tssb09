%function show_origin(origin,xdirn,ydirn,l)
%
% Visualize origin and X and Y axes
%
%
% Per-Erik Forssen, April 2003
% [Mod by ErikJ, 2006]
function show_origin(origin,xdirn,ydirn)
 
if nargin<4,l=100;end

% xdirn=xdirn/sqrt(xdirn'*xdirn);
% ydirn=ydirn/sqrt(ydirn'*ydirn);

%Plot X-axis
h=plot(origin(1)+3*[0 xdirn(1)],origin(2)+3*[0 xdirn(2)],'r');
set(h,'LineWidth',2.0);

%Plot Y-axis
h=plot(origin(1)+2*[0 ydirn(1)],origin(2)+2*[0 ydirn(2)],'r');
set(h,'LineWidth',2.0);
