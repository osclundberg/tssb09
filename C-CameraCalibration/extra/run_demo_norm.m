%
% This program reads four stored images
% and applies camera calibration according to Zhang's method
%
% Maria Magnusson 2007-11-01
%

clear Xplane;
clear XImg;

figure(1)
img = imread('im0.png');
img = rgb2gray(im2double(img));
[P_img, P_world] = detect_L_pattern(img, [], 1);
Xplane{1} = P_world;
XImg{1}   = P_img;

figure(2)
img = imread('im1.png');
img = rgb2gray(im2double(img));
[P_img, P_world] = detect_L_pattern(img, [], 1);
Xplane{2} = P_world;
XImg{2}   = P_img;

figure(3)
img = imread('im2.png');
img = rgb2gray(im2double(img));
[P_img, P_world] = detect_L_pattern(img, [], 1);
Xplane{3} = P_world;
XImg{3}   = P_img;

figure(4)
img = imread('im3.png');
img = rgb2gray(im2double(img));
[P_img, P_world] = detect_L_pattern(img, [], 1);
Xplane{4} = P_world;
XImg{4}   = P_img;

% Run Zhang's calibration method
[A, R, t] = CameraCalibration(Xplane, XImg, 1);
A
R
t
