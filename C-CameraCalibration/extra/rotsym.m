function [RS,dIm] = rotsym(dIm,orders,inhib,s_rs,s_grad,a)

% [RS,dIm] = rotsym(signal,orders,inhib,s_rs,s_grad,a)
%
% Detect rotational symmetries of different orders in an image.
% OBS! The result is in Matlabs own coordinate system,
% i.e. x1+i*x2, not Cartesian x+i*y.
%
%  signal - M1/M2 grayvalued image or
%           M1/M2/2 image gradient in Matlab coordinates
%  orders - 1/N vector of desired symmetry orders in the range [-4 4]
%   inhib - 1/K vector specifying which orders to use for inhibition
%           Inhibition orders can be other than the output orders
%           (default inhib=[])
%    s_rs - rotational symmetry size (std of a Gaussian window)
%           (default s_rs=2)
%  s_grad - gradient size (std of a Gaussian window)
%           Not used if signal is a gradient image
%           (default s_grad=1)
%       a - Gradient magnitude sensitivity
%           Not used if signal is a gradient image
%           (default a=1)
%
%      RS - M1/M2/N complex valued matrix, where RS(:,:,k) is the
%           symmetry of order(k) 
%     dIm - M1/M2/2 image gradient
%
%
%
% Author: Bj�rn Johansson
%         Computer Vision Laboratory
%         Link�ping University, Sweden
%         bjorn@isy.liu.se
%         2006-10-23

[M1 M2 M3] = size(dIm);
N = length(orders);

if nargin<2
  error('orders must be specified!');
end
if nargin<3 | isempty(inhib), inhib = [];end
if nargin<4 | isempty(s_rs), s_rs = 2;end
if nargin<5 | isempty(s_grad), s_grad = 1;end
if nargin<6 | isempty(a), a = 1;end

if ~isempty(inhib)
  % Add the inhibition orders that are missing
  offset = 1-min(min(orders),min(inhib));
  c = zeros(1,max(max(orders),max(inhib))+offset);
  c(orders+offset) = 1;
  orders = [orders inhib(c(inhib+offset)==0)];
  dN = length(orders)-N;
else
  dN = 0;
end

%----------------------------------
% Compute image gradient if needed
%----------------------------------

if M3==1
  dIm = image2grad(dIm,s_grad,[],a);

  % Remove border effects
  d=ceil(s_grad*sqrt(2*log(500)));
  dIm([1:d end-d+1:end],:,:) = 0;
  dIm(:,[1:d end-d+1:end],:) = 0;
end

%------------------------
% Compute needed moments
%------------------------

abs_orders = unique(abs(orders));
moments = cell(2,1+max(abs_orders));
for k=abs_orders
  basis = get_filterbasis(k);
  moments{1,k+1} = local_moments(dIm(:,:,1).^2-dIm(:,:,2).^2,basis,[],s_rs);
  moments{2,k+1} = local_moments(dIm(:,:,1).*dIm(:,:,2),basis,[],s_rs);
end

%--------------------------------------------
% Compute rotational symmetries from moments
%--------------------------------------------

RS = zeros(M1,M2,N+dN);
for n=1:N+dN
  coeff = get_filterweights(orders(n));
  for k=1:length(coeff)
    RS(:,:,n) = RS(:,:,n) + ...
        coeff(k)*(moments{1,1+abs(orders(n))}(:,:,k) + ...
                  (2*i)*moments{2,1+abs(orders(n))}(:,:,k));
  end
end

%-------------------------------
% Compute inhibition if desired
%-------------------------------

if ~isempty(inhib)
  % Compute certainty
  absz = dIm(:,:,1).^2+dIm(:,:,2).^2;
  c = local_moments(absz,[0;0],[],s_rs) + eps;

  % Compute inhibition factors
  offset = 1-min(inhib);
  Inh = cell(1,max(inhib)+offset);
  for k=inhib
    f = get_inhibition_factor(k,s_rs);
    ind = find(k==orders);
    inhk = max(0,1-f*abs(RS(:,:,ind))./c);
    Inh{k+offset} = inhk;
  end

  % Inhibit
  for n=1:N
    for k=inhib
      if k~=orders(n)
        RS(:,:,n) = RS(:,:,n).*Inh{k+offset};
      end
    end
  end

  % Remove extra inhibition orders
  if dN>0
    RS = RS(:,:,1:N);
  end
end

return
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function f = get_inhibition_factor(N,sigma)

% Compute suitable ad-hoc inhibition factor for order N
% based on the maximum of the inhibition value
%
%          |N|  i*n*fi            |N|
%      <g*r  * e      ,z>     <g*r   ,1>
%      ------------------  ~  ----------
%            <g,|z|>            <g,1>

switch abs(N)
 case 0
  f = 1;
 case 1
  f = sqrt(pi)/sqrt(pi)*sigma;
 case 2
  f = 2*sigma^2;
 case 3
  f = 3*sqrt(pi)/sqrt(2)*sigma^3;
 case 4
  f = 8*sigma^4;
 case 5
  f = 15*sqrt(pi)/sqrt(2)*sigma^5;
 case 6
  f = 48*sigma^6;
end
f = 1/f;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function symbol_inhibition(N)

% Symbolic generation of the inhibition factors

syms r s real;
g = exp(-r^2/s^2/2);

f = r^N*g;
s = simplify(int(f*r,'r',0,inf)/int(g*r,'r',0,inf))

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% The following functions computes the moment orders and
% corresponding weight coefficients for computation of the
% rotational symmetry of order N
%
% Example:
%               2              2          2
%   s2 = <(x+iy) , z> = <(x+iy) , (fx+ify) > =
%          2   2           2    2
%      = <x - y + i2xy , fx - fy + i2fxfy> =
%
%
%  therefore:         [2 1 0]
%             basis = [0 1 2]     c = [1 -2i -1]
%
%  (The complex valued scalar product is defined as <a,b> = sum(conj(a)*b))

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function basis = get_filterbasis(N)

% Computes moment orders needed for computation of the rotational
% symmetry of order N

basis = [N:-1:0;0:N];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function coeff = get_filterweights(N)

% Computes weight coefficients for the moments for
% computation of the rotational symmetry of order N

switch N
 case -4
  coeff = [1 4*i -6 -4*i 1];
 case -3
  coeff = [1 3*i -3 -i];
 case -2
  coeff = [1 2*i -1];
 case -1
  coeff = [1 i];
 case 0
  coeff = [1];
 case 1
  coeff = [1 -i];
 case 2
  coeff = [1 -2*i -1];
 case 3
  coeff = [1 -3*i -3 i];
 case 4
  coeff = [1 -4*i -6 4*i 1];
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function symbol_weights(N)

% Symbolic generation of the weight coefficients 
% This function is only used when writing function
% get_filterweights

syms x y real;

h = expand((x-sign(N)*i*y)^abs(N));
