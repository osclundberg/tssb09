function [P_img, P_world, origin, xdirn, ydirn] = detect_L_pattern(img, params, doplot)
%
%  [P_img, P_world, origin, xdirn, ydirn] = detect_L_pattern(img, params, doplot)
%
%  Detects the L-shaped calibration pattern found in grid1cmA4.eps and
%  returns the image coordinates of each grid point and two coordinate
%  system axes.
%
%  Parameters:
%    img:     Grayscale input image containing an L-pattern, approximately 
%             centered. Should be a grayscale double ([0..1]) image.
%    params:  [s, bdz, dmax], where
%             s = feature size
%             bsz = image border size in pixels
%             dmax = sensitivity treshold for generate_correspondence.
%             If omitted or empty, params defaults to [3, 10, 0.11]
%    doplot:  Flag. If 1, plots the results, otherwise is silent (default
%             is 0)
%
%  Return values:
%    P_img:   Nx2 matrix of image coords of grid points (x,y)
%    P_world: Nx2 matrix of world coords of grid points (x,y)
%    origin:  [x,y] image coordinate of the world origin
%    xdirn:   [x,y] vector pointing along the x axis
%    ydirn:   [x,y] vector pointing along the y axis
%
%  Example:
%
%    img = rgb2gray(im2double(imread('target_image.jpg')));
%    [P_img, P_world] = detect_L_pattern(img, [], 1);
%
%  [ErikJ 2006, restructured version of PEs code]
%

if nargin<2 || isempty(params)
  s    = 4;    % feature size (std of Gaussian window)
  bdz  = 10;   % Image border size
  dmax = 0.2; % Sensitivity threshold for generate_correspondence
              % Was 0.11
else
  s = params(1);
  bdz = params(2);
  dmax = params(3);
end

if nargin<3
  doplot = 0;
end

%%% Extracting circle and star patterns
% Using old rotsym function
% [RS,z] = image2rotsym(img,s,[],[],0);   % Non-inhibited symmetries
% RS2 = rotsym_order2inhibit(RS,z,s);

% Using new rotsym
RS2 = rotsym(img, 2, [0 1], s);

rs1 = max(0,real(RS2));  % Extract circle patterns
rs2 = max(0,-real(RS2)); % Extract star patterns

%%% Extracting lists of points and finding correspondence
% First extract origin. Should be close to the center of the image, so
% use a rather large border size (1/4 of the image size)
bdz2 = round(min(size(img)/4));
[X,Y,V] = extract_pointlist(rs1,bdz2);
[Y,X] = refine_positions(Y,X,rs1);  % NOTE: The X,Y order has changed!
[origin, xdirn, ydirn]=extract_origin(X,Y,V);
% Now find intersection points and generate corresponding world coordinates
[X,Y] = extract_pointlist(rs2, bdz);
[Xi,Yi,Xw,Yw] = generate_correspondence(X,Y,origin,xdirn,ydirn,dmax);
[Yi,Xi] = refine_positions(Yi,Xi,rs2); % NOTE: the X,Y order has changed!

if doplot
  clf;
  subplot(1,1,1); imagesc(img); axis image;
  colormap(gray(256));

  %  subplot(2,2,2);gopimage(RS2 / max(RS2(:)),1);axis image;
  %  title('Inhibited symmetry, order 2');

  %  subplot(2,2,3);
  %  imagesc(rs1);axis image
  %  title('Circle patterns');

  %  subplot(2,2,4);
  %  imagesc(rs2);axis image
  %  colormap(gray(256))
  %  title('Star patterns');

  subplot(1,1,1);hold on
  h = plot(Xi,Yi,'g.');
  set(h,'MarkerSize',16);

  show_origin(origin, xdirn, ydirn);
  title('Found points(GREEN), coordinate system(RED)');
end

P_img = [Xi Yi];
P_world = [Xw Yw];

