%function [X,Y,V]=extract_pointlist(Im,bdsz,emin)
%
% Extract list of coordinates of local peaks
% in corner image Im.
%
% BDSZ Border size. Points closer than BDSZ to
%      border are neglected. (default 10)
% EMIN minimum energy required as fraction of
%      highest energy in Im (default 0.05)
%
%Per-Erik Forssen, March 2003

function [X,Y,V]=extract_pointlist(Im,bdsz,emin)

if nargin<2,bdsz=10;end
if nargin<3,emin=0.05;end

[yz,xz]=size(Im);

ethr=emin*max(Im(:));

[Y,X,V]=find(non_max_suppression(Im.*(Im>ethr),9));

keep_list=(X>bdsz)&(X<xz-bdsz)&(Y>bdsz)&(Y<yz-bdsz);
Y=Y(keep_list);
X=X(keep_list);
V=V(keep_list);

