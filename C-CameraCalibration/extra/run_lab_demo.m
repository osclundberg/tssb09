%
% This program reads four stored images
% and applies camera calibration according to Zhang's method
%
% Maria Magnusson 2007-11-01
%

clear Xplane;
clear XImg;

figure(1)
img = imread('bild0.jpg');
img = rgb2gray(im2double(img));
[P_img, P_world] = detect_L_pattern(img, [], 1);
Xplane{1} = P_world;
XImg{1}   = P_img;

figure(2)
img = imread('bild1.jpg');
img = rgb2gray(im2double(img));
[P_img, P_world] = detect_L_pattern(img, [], 1);
Xplane{2} = P_world;
XImg{2}   = P_img;

figure(3)
img = imread('bild2.jpg');
img = rgb2gray(im2double(img));
[P_img, P_world] = detect_L_pattern(img, [], 1);
Xplane{3} = P_world;
XImg{3}   = P_img;

figure(4)
img = imread('bild3.jpg');
img = rgb2gray(im2double(img));
[P_img, P_world] = detect_L_pattern(img, [], 1);
Xplane{4} = P_world;
XImg{4}   = P_img;

% Run Zhang's calibration method
[A1, R1, t1] = CameraCalibration(Xplane, XImg, 1)
[A2, R2, t2] = CameraCalibration(Xplane, XImg, 2)
[A3, R3, t3] = CameraCalibration(Xplane, XImg, 3)
[A4, R4, t4] = CameraCalibration(Xplane, XImg, 4)
