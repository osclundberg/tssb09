%function [Y,X,dy,dx] = refine_positions(Y,X,Im,sz,mode)
%
% Refine positions of a list of maxima using sub-pixel 
% interpolation in feature image Im.
%
% Parameters:
%   sz    Sub-pixel interpolation window size
%         0: no interpolation. Must be 0 or odd. 
%         Default is 5.
%
%   mode: Determines the type of polynomial
%        'full': Uses all degrees of freedom, i.e. 
%                f(x,y) = [x y]*A*[x;y] + b'*[x;y] + c
%
%        'eye': Uses a constrained polynomial with
%               A = a*eye(2) (more stable, but less
%               exact)
%        Default is 'eye'.
%
% Return value:
%   Y, X:  Refined positions
%   dy,dx: Subpixel improvemets, i.e. X=X0+dx, Y=Y0+dx
%
% Note: For historical reasons, the 'full' mode uses 
% polyexp on the entire input image, which can be expected
% to be slower than 'eye', which only processes a point list.
% If anyone needs speed, the 'full' version should be rewritten
% such that is also uses point lists.
%
% [Restructured by ErikJ 2006, based on code from 
%  Per-Erik Forssen and Bj�rn Johansson]
function [Y,X,dy,dx] = refine_positions(Y,X,Im,sz,mode)

if nargin<4, sz=5; end
if nargin<5, mode='eye'; end

K = size(X,1);

% Trivial special case: do nothing
if sz==0
  return;
end

if mod(sz,2) == 0
  error('sz must be 0 or odd');
end

switch mode

  % Interpolate using A = a*eye(2). Based on Per-Eriks old
  % 'refine_positions', 'find_peak3' and 'find_peak5'. Loops
  % through all points in the image
  case 'eye'

    % Define the interpolation matrix
    Wp = interpol_matrix(sz);
    
    halfsz = (sz-1)/2;
    for k = 1:K  % For each point...
      try
        % Cut out the local region and find the expansion coefficients
        M = Im(Y(k)+[-halfsz:halfsz], X(k)+[-halfsz:halfsz]);
        coeff = Wp*M(:);

        % Compute the offset and store the result
        dx(k) = coeff(2) / coeff(1);
        dy(k) = coeff(3) / coeff(1);
        X(k) = X(k) + dx(k);
        Y(k) = Y(k) + dy(k);
      catch
        % If something goes wrong here, we're indexing outside
        % the image range. In that case, just keep the old position
        dx(k) = 0;
        dy(k) = 0;
      end
    end

    
  % Interpolate using a full polynomial. Based on Bj�rn's old
  % non_max_suppression_sp
  case 'full'
    % Hard-coded to use a gauss-std of sz/10 for the applicability.
    % FIXME: Make this a parameter?
    % It would be faster to use a point list here as well...
    r = polyexp(Im,[],'quadratic',sz,sz/10);
    detA = r(:,:,4).*r(:,:,5) - r(:,:,6).^2/4;
    dy = -(r(:,:,2).*r(:,:,5) - r(:,:,3).*r(:,:,6)/2)/2./(detA+eps);
    dx = -(r(:,:,3).*r(:,:,4) - r(:,:,2).*r(:,:,6)/2)/2./(detA+eps);
    
    IND = sub2ind(size(Im), Y, X);
    
    Y = Y + dy(IND);
    X = X + dx(IND);
    
    dy = dy(IND);
    dx = dx(IND);
    
  otherwise
    error('Unknown method');
    
end
   


function Wp = interpol_matrix(sz)
%  Based on Per-Erik's old find_peak3 and find_peak5

if sz==3,
  % Precalculated, for speed (only calculated once anyway, so no
  % large advantage)
  Wp = [ 6 -3  6 -3 -12 -3  6 -3  6 ;
         3  3  3  0   0  0 -3 -3 -3 ;
         3  0 -3  3   0 -3  3  0 -3 ;
        -4  8 -4  8  20  8 -4  8 -4] / 36;
else
  halfsz = (sz-1)/2;
  [X,Y] = meshgrid([-halfsz:halfsz],[-halfsz:halfsz]);
  X = X(:);
  Y = Y(:);
  W = [X.^2+Y.^2, -2*X, -2*Y, ones(sz^2,1)];
  Wp = pinv(W);
end
      
