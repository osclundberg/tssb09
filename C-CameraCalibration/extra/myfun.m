function err = myfun(param, Xplane, XImg)

% Unpack parameters
%------------------
A = eye(3,3);
A(1,1) = param(1);
A(2,2) = param(2);
A(1,2) = param(3);
A(1,3) = param(4);
A(2,3) = param(5);
R1 = reshape(param(6:14),[3 3]);
R2 = reshape(param(15:23),[3 3]);
R3 = reshape(param(24:32),[3 3]);
R4 = reshape(param(33:41),[3 3]);
t1 = param(42:44)';
t2 = param(45:47)';
t3 = param(48:50)';
t4 = param(51:53)';

% Make R a true Rotation matrix
%------------------------------
[U,D,V] = svd(R1);
R1 = U*V';
[U,D,V] = svd(R2);
R2 = U*V';
[U,D,V] = svd(R3);
R3 = U*V';
[U,D,V] = svd(R4);
R4 = U*V';

% Loop over all points
%---------------------
err = []; % start with an empty error vector
for p = 1:1,
    XY = Xplane{p};
    UV = XImg{p};
    if (p==1) R = R1; t = t1; end
    if (p==2) R = R2; t = t2; end
    if (p==3) R = R3; t = t3; end
    if (p==4) R = R4; t = t4;
    end
    for k = 1:7:size(XY,1),
	XYZ1 = [XY(k,:) 0 1]';
        uv = [UV(k,:)]';
        sm = A * [R t] * XYZ1;
        m = sm(1:2)./sm(3);
        err = [err; m-uv]; % accumulate error vector
    end
end
