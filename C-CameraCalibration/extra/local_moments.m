function moments = local_moments(signal, basis, spatial_size, sigma, options)
% MOMENTS = LOCAL_MOMENTS(SIGNAL, ORDERS, SPATIAL_SIZE, SIGMA, OPTIONS)
% 
% Compute local moments in one, two, or three dimensional signals.
% For example, in 2D:
%
%            //          p q
%      Mpq = ||  g(x,y) x y s(x,y)dxdy , where s is the signal and
%            //                                g is a Gaussian weight
%
% SIGNAL
%   Signal values. Must be real and nonsparse and the number of dimensions,
%   N, must be one, two, or three.
%
%   N.B. 1D signals must be represented as column vectors. Row vectors
%        are interpreted as (very narrow) 2D signals.
%
% ORDERS [optional]
%   Set of moments to use. This can be either a string or a matrix.
%   In the former case valid values are 'zeroth', 'first',
%   'second', and 'third'. In the latter case the matrix must be of
%   size NxM, where N is the signal dimensionality and M is the
%   number of moments. The meaning of this parameter is
%   further discussed below. The default value is 'second'.
%
% SPATIAL_SIZE [optional]
%   Size of the spatial support of the filters along each dimension. Must be
%   odd. Default value is a function of SIGMA if existing, 9 otherwise.
%
% SIGMA [optional]
%   Standard deviation of a Gaussian applicability. The default value is
%   0.15(K-1), where K is the SPATIAL_SIZE.
% 
% OPTIONS [optional]
%   Struct array that may contain various parameters that affect the
%   algorithm. These are explained below. The default value is no options.
%
% MOMENTS
%   Computed expansion coefficients. MOMENTS has N+1 dimensions, where the first
%   N indices indicate the position in the signal and the last dimension holds
%   the expansion coefficients. In the case that OPTIONS.region_of_interest
%   is less than N-dimensional, the singleton dimensions are removed.
%
% 
% The optional parameters can safely be omitted from the right. In some
% cases the code can detect early omissions, such as the fact that the last
% parameter always can be assumed to be OPTIONS if it is a struct array. If
% you are not sure whether the code would guess right, include all
% intermediate parameters.
%
%
% The purpose of the ORDERS parameter is to specify which moments
% are to be used. A good example are the second order moments in 2D,
% consisting of the functions {x^2, y^2, xy}. Here x is understood
% to be increasing along the first dimension of the signal. (Notice
% that this with the common visalization of images would mean
% downwards, not rightwards.) Naturally y then increases along the
% second dimension of the signal. Both variables take integer
% values in the interval [-(K-1)/2, (K-1)/2], where K is the
% SPATIAL_SIZE. The ordering of the moments in MOMENTS of course
% follows the moment functions.
%
% The moments are uniquely determined by the exponents for each
% variable. Therefore the moments up to the second order in 2D may
% be represented by the 2x6 matrix
%
%   0 1 0 2 0 1
%   0 0 1 0 2 1
% 
% Thus by letting ORDERS be an NxM matrix, arbitrary moments
% constructed. A special convention is that an empty matrix is
% interpreted as the default moments up to the second order.
%
% The exact meaning of 'zeroth', 'first', 'second', and 'third' in
% the various dimensionalities is specified in this table of
% equivalent moment matrices:
%
% Dimensionality:   1                 2                    3
%
%                   0                 0                    0
% 'zeroth'                            0                    0
%                                                          0
%
%                  0 1              0 1 0               0 1 0 0
% 'first'                           0 0 1               0 0 1 0
%                                                       0 0 0 1
%
%                 0 1 2          0 1 0 2 0 1      0 1 0 0 2 0 0 1 1 0
% 'second'                       0 0 1 0 2 1      0 0 1 0 0 2 0 1 0 1
%                                                 0 0 0 1 0 0 2 0 1 1
%
%                0 1 2 3     0 1 0 2 1 0 3 2 1 0
% 'third'                    0 0 1 0 1 2 0 1 2 3
%                                                   
%                 		         0 1 0 0 2 0 0 1 1 0 3 0 0 2 2 1 0 1 0
%                 		         0 0 1 0 0 2 0 1 0 1 0 3 0 1 0 2 2 0 1
%                 		         0 0 0 1 0 0 2 0 1 1 0 0 3 0 1 0 1 2 2
% 
%
% The following fields may be specified in the OPTIONS parameter:
% OPTIONS.region_of_interest -
%                 An Nx2 matrix where each row contains start and stop
%                 indices along the corresponding dimensions. Default value
%                 is all of the signal.
%
% OPTIONS.applicability -
%                 The default applicability is a Gaussian determined by the
%                 SPATIAL_SIZE and SIGMA parameters. This field overrules
%                 those settings. It can either be an array of the same
%                 dimensionality as the signal, or a cell array containing
%                 one 1D vector for each signal dimension. The latter 
%                 can be used when the applicability is separable and allows
%                 more efficient computations.
%                 
% OPTIONS.save_memory -
%                 If existing and nonzero, do not use the separable methods
%                 even if the applicability is separable. This saves memory
%                 while increasing the amount of computations.
%
% OPTIONS.verbose -
%                 If existing and nonzero, print a report on how the
%                 parameters have been interpreted and what method is going
%                 to be used. This is for debugging purposes and can only be
%                 interpreted in the context of the actual code for this
%                 function.
%
%
% Author: Bj�rn Johansson
%         Computer Vision Laboratory
%         Link�ping University, Sweden
%         bjorn@isy.liu.se
%
% Basically a modification of polyexp.m by Gunnar Farneb�ck
% (it is not possible to include a certainty here)

% We are going to modify the value returned by nargin, so we copy it to a
% variable.
numin = nargin;
    
if numin < 1
  error('missing parameters')
end

% 1D signals are represented by column vectors. However, ndims() never
% reports fewer than 2 dimensions, so we need to detect 1D signals
% explicitly.
N = ndims(signal);
if N == 2 & size(signal, 2) == 1
  N = 1;
end

% Is the last parameter a struct? Then assume it is the options parameter.
% First set options to an empty array if it has not appeared in its own
% position.

if numin < 5
  options = [];
end

if numin == 2 & isstruct(basis)
  options = basis;
  numin = numin - 1;
end

if numin == 3 & isstruct(spatial_size)
  options = spatial_size;
  numin = numin - 1;
end

if (numin == 4 & isstruct(sigma))
  options = sigma;
  numin = numin - 1;
end

% Add default values for other missing parameters than options.
if numin < 2
  basis = 'second';
end

if numin < 3 | isempty(spatial_size)
  if numin < 4 | isempty(sigma)
    spatial_size = 9;
  else
    spatial_size = 1+2*ceil(sigma*sqrt(2*log(100)));
  end
end

if numin < 4 | isempty(sigma)
  sigma = 0.15 * (spatial_size - 1);
end

% All input parameters have now been identified and assigned default values
% if they were missing. Check the validity of certain parameters.

if spatial_size < 1
  error('What use would such a small kernel be?')
elseif mod(spatial_size, 2)~=1
  spatial_size = 2*floor((spatial_size-1)/2) + 1;
  warning(sprintf('Only kernels of odd size are allowed. Changed the size to %d.', spatial_size))
end

% Construct a region of interest, using the one in options if provided.

if isfield(options, 'region_of_interest')
  region_of_interest = options.region_of_interest;
else
  if N == 1
    region_of_interest = [1 size(signal, 1)];
  else
    region_of_interest = [ones(N, 1), size(signal)'];
  end
end

% Construct applicability. If one is given in options we use that one.
% Otherwise we build a Gaussian with the specified spatial size and standard
% deviation.

if isfield(options, 'applicability')
  applicability = options.applicability;
else
  n = (spatial_size - 1) / 2;
  a = exp(-(-n:n).^2/(2*sigma^2))';
  a = a/sum(a);
  if N == 1
    applicability = a;
  elseif N == 2
    applicability = {a, a'};
  elseif N == 3
    applicability = {a, a', shiftdim(a, -2)};
  end
end

% Check that the applicability looks reasonable
if iscell(applicability)
  if length(applicability) ~= N
    error('separable applicability inconsistent with signal')
  end
  
  for k = 1:length(applicability)
    if sum(size(applicability{k}) ~= 1) > 1
      error('separable applicability must consist of vectors');
    end
    % Make sure the directions are right.
    a = applicability{k};
    applicability{k} = shiftdim(a(:), -(k-1));
  end
end

% Moments. If given as string, convert to matrix form.
% A special convention is that an empty matrix is interpreted as
% the default moments (second).
if isempty(basis)
  basis = 'second';
end

if ischar(basis)
  switch basis
   case 'zeroth'
    basis = zeros(N, 1);
   case 'first'
    if N == 1
      basis = [0 1];
    elseif N == 2
      basis = [0 1 0
	       0 0 1];
    elseif N == 3
      basis = [0 1 0 0
	       0 0 1 0
	       0 0 0 1];
    end
   case 'second'
    if N == 1
      basis = [0 1 2];
    elseif N == 2
      basis = [0 1 0 2 0 1
	       0 0 1 0 2 1];
    elseif N == 3
      basis = [0 1 0 0 2 0 0 1 1 0
	       0 0 1 0 0 2 0 1 0 1
	       0 0 0 1 0 0 2 0 1 1];
    end
   case 'third'
    if N == 1
      basis = [0 1 2 3];
    elseif N == 2
      basis = [0 1 0 2 1 0 3 2 1 0
	       0 0 1 0 1 2 0 1 2 3];
    elseif N == 3
      basis = [0 1 0 0 2 0 0 1 1 0 3 0 0 2 2 1 0 1 0
	       0 0 1 0 0 2 0 1 0 1 0 3 0 1 0 2 2 0 1
	       0 0 0 1 0 0 2 0 1 1 0 0 3 0 1 0 1 2 2];
    end
   otherwise
    error('unknown basis name')
  end
else
  if size(basis, 1) ~= N
    error('basis and signal inconsistent');
  end
end

% Decide method.

if iscell(applicability)
  separable_computations = 1;
else
  separable_computations = 0;
end

if isfield(options, 'save_memory') & options.save_memory ~= 0
  separable_computations = 0;
end

if separable_computations
  method = 'SC';
else
  method = 'C';
end

% If we are not going to do separable computations but we have a separable
% applicability, collapse it to an array.
if ~separable_computations & iscell(applicability)
  a = applicability{1};
  for k = 2:N
    a = outerprod(a, applicability{k});
  end
  applicability = a;
end

% Set up the moment coordinates. If we do separable computations, these
% are vectors, otherwise full arrays.
for k = 1:N
  if iscell(applicability)
    n = (length(applicability{k}) - 1) / 2;
  else
    n = (size(applicability, k) - 1) / 2;
  end
  X{k} = shiftdim((-n:n)', -(k-1));
end

if ~separable_computations
  if N == 2
    x1 = X{1};
    x2 = X{2};
    X{1} = outerprod(x1, ones(size(x2)));
    X{2} = outerprod(ones(size(x1)), x2);
  elseif N == 3
    x1 = X{1};
    x2 = X{2};
    x3 = X{3};
    X{1} = outerprod(outerprod(x1, ones(size(x2))), ones(size(x3)));
    X{2} = outerprod(outerprod(ones(size(x1)), x2), ones(size(x3)));
    X{3} = outerprod(outerprod(ones(size(x1)), ones(size(x2))), x3);
  end
end



% The caller wants a report about what we are doing.
if isfield(options, 'verbose') & options.verbose ~= 0
  disp(sprintf('method: %s', method));
  disp('orders:');
  disp(basis);
  disp('applicability:');
  disp(applicability);
  disp('region_of_interest:');
  disp(region_of_interest);
  for k = 1:N
    disp(sprintf('X%d:\n', k));
    disp(X{k});
  end
end


%%%% Now over to the actual computations. %%%%

M = size(basis, 2);

if strcmp(method, 'C')
  % Convolution. Compute the metric G and the equivalent correlation
  % kernels.
  B = zeros([prod(size(applicability)) M]);
  for j = 1:M
    b = ones(size(applicability));
    for k = 1:N
      b = b .* X{k}.^basis(k, j);
    end
    B(:, j) = b(:);
  end
  W = diag(sparse(applicability(:)));
  B = W * B;
  
  % Now do the correlations to get the expansion coefficients.
  moments = zeros([prod(1+diff(region_of_interest')) M]);
  for j = 1:M
    coeff = conv3(signal, reshape(B(:,j), size(applicability)), region_of_interest);
    moments(:,j) = coeff(:);
  end
  moments = reshape(moments, [1+diff(region_of_interest') M]);
  
else
  % Separable Convolution. This implements the generalization of figure
  % 4.9 to any dimensionality and any choice of moments
  % Things do become fairly intricate.
  convres = cell(1 + max(basis'));
  sorted_basis = sortrows(basis(end:-1:1, :)')';
  sorted_basis = sorted_basis(end:-1:1, end:-1:1);
  convres{1} = signal;

  % We start with the last dimension because this can be assumed to be
  % most likely to have a limited region of interest. If that is true this
  % ordering saves computations. The correct way to do it would be to
  % process the dimensions in an order determined by the region of
  % interest, but we avoid that complexity for now. Notice that the
  % sorting above must be consistent with the ordering used here.
  for k = N:-1:1
    % Region of interest must be carefully computed to avoid needless
    % loss of accuracy.
    roi = region_of_interest;
    for l = 1:k-1
      roi(l, 1) = roi(l, 1) + min(X{l});
      roi(l, 2) = roi(l, 2) + max(X{l});
    end
    roi(:, 1) = max(roi(:, 1), ones(N, 1));
    roi(:, 2) = min(roi(:, 2), size(signal)');
    % We need the index to one of the computed correlation results
    % at the previous level.
    index = sorted_basis(:, 1);
    index(1:k) = 0;
    index = num2cell(1 + index);
    % The max(find(size(...)>1)) stuff is a replacement for ndims(). The
    % latter gives a useless result for column vectors.
    roi = roi(1:max(find(size(convres{index{:}}) > 1)), :);
    if k < N
      koffset = roi(k, 1) - max(1, roi(k, 1) + min(X{k}));
      roi(:, :) = roi(:, :) + 1 - repmat(roi(:, 1), [1 2]);
      roi(k, :) = roi(k, :) + koffset;
    end
    % 	  roi(k+1:end, :) = (roi(k+1:end, :) + 1 - ...
    % 			     repmat(roi(k+1:end, 1), [1 2]));
    last_index = sorted_basis(:, 1) - 1;
    for j = 1:M
      index = sorted_basis(:, j);
      e = index(k);
      index(1:k-1) = 0;
      if ~all(index == last_index)
	last_index = index;
	index = num2cell(1 + index);
	prior = index;
	prior{k} = 1;
        convres{index{:}} = conv3(convres{prior{:}}, ...
                                  applicability{k} .* X{k}.^e, ...
                                  roi);
      end
    end
  end

  % Put into result array
  moments = zeros([prod(1+diff(region_of_interest')) M]);
  for j = 1:M
    index = num2cell(1 + basis(:, j));
    moments(:,j) = convres{index{:}}(:);
  end
  moments = reshape(moments, [1+diff(region_of_interest') M]);
end

return

%%%% Helper functions. %%%%

% Compute outer product of two arrays. This only works correctly if they
% have mutually exclusive non-singleton dimensions.
function z = outerprod(x, y)
  z = repmat(x, size(y)) .* repmat(y, size(x));
return
