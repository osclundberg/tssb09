Jag har modifierat Eriks program run_lab.m till run_lab2.m.
run_lab2.m anropar CameraCalibration.m ist�llet f�r calib_zhang_simple.m.
CameraCalibration.m �r �nd� enklare �n calib_zhang_simple.m och
st�mmer med koden i labh�ftet. Det finns ocks� tre program
som l�ser lagrade bilder ist�llet f�r bilder fr�n kameran.
De heter run_lab_demo.m, run_demo_norm.m och run_demo_zoom.m

/Maria Magnusson, 2007-11-11 och 2010-11-07

##############################################################################

Denna katalog inneh�ller all matlab-kod som beh�vs f�r att k�ra 
demonstrationen av kamerakalibrering i Bildanalys-kursen.

All kod �r direkt kopierad fr�n CVL's matlab-repository, f�rutom run_lab.m som
�r specialskriven f�r denna labben.


OBS:
Om n�gon hittar n�gon bug i den h�r koden �r det FRAMF�RALLT i CvlMatlab-
repositoryt som buggen ska r�ttas, eftersom det �r d�r huvudversionerna av
filerna finns. N�r buggen r�ttats d�r kan nya versionen kopieras in hit. Denna
katalog b�r alltid bara vara en spegling av CvlMatlab-repositoryt, f�r att
undvika att ha flera versioner av samma filer som driftar is�r.

Dessutom, det kan vara bra att med j�mna mellanrum kontrollera s� att
ingenting har uppdaterats i CvlMatlab utan att uppdateras h�r. 

Denna l�sning har gjorts f�r att slippa checka ut hela CvlMatlab-tr�det p�
de datorer som labben ska k�ras p�, plus att slippa att labben pajar f�r att
n�gon g�r en icke-bak�t-kompatibel �ndring i CvlMatlab.


/Erik, 2007-10-10

