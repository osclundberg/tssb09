function im = cvlcam00(ptz)

% function im = cvlcam00(ptz)
%
% This function retrieves an image from the Axis PTZ network
% camera. The PTZ (Pan, Tilt, Zoom), iris, and focus parameters can
% (optionally) be set.
%
% IM             The retrieved image.
% PTZ.PAN        Pan relative to the (0,0) position. [-180.0,180.0] 
% PTZ.TILT       Tilt relative to the (0,0) position. [-180.0,180.0] 
% PTZ.ZOOM       Zoom the device n steps (absolute). [1,9999]
% PTZ.FOCUS      Focus the device n steps (absolute). [1,9999]
% PTZ.IRIS       Move iris n steps (absolute). [1,9999]
% PTZ.RPAN       Pan relative to the current position. [-360.0,360.0] 
% PTZ.RTILT      Tilt relative to the current position. [-360.0,360.0] 
% PTZ.RZOOM      Zoom n steps relative to the current pos. [-9999,9999]
% PTZ.RFOCUS     Focus n steps relative to the current pos. [-9999,9999]
% PTZ.RIRIS      Iris n steps relative to the current pos. [-9999,9999]
% PTZ.AUTOFOCUS  Set autofocus on or off. ['on','off']
% PTZ.AUTOIRIS   Set autoiris on or off. ['on','off']
% PTZ.CENTER     Pan/tilt to center at the image coordinate. [<x>,<y>]
%
% Author: Johan Wiklund, jowi@isy.liu.se
%
% File revision $Id$
%

camadr = 'http://cvl-cam-00.edu.isy.liu.se/';
ptzgrp = 'axis-cgi/com/ptz.cgi?';
largejpg = 'jpg/hugesize.jpg';
smalljpg = 'jpg/fullsize.jpg';
largesize = '&imagewidth=704&imageheight=480';
smallsize = '&imagewidth=352&imageheight=240';

if nargin < 1
  % Read image from camera and return
  im = imread([camadr largejpg]);
  return
end  

fnam = {'pan','tilt','zoom','focus','iris',...
        'rpan','rtilt','rzoom','rfocus','riris',...
        'autofocus','autoiris','center'};

tf = isfield(ptz, fnam);
idx = find(tf);
for e=1:length(idx)
  if isempty(ptz.(fnam{idx(e)}))
    tf(e) = 0;
  end
end
idx = find(tf);

cmd = [camadr ptzgrp];
delim = '';

for e=1:length(idx)
  val = ptz.(fnam{idx(e)});
  if idx(e) == 11 | idx(e) == 12
    cmd = [cmd delim fnam{idx(e)} '=' val];
  else
    cmd = [cmd delim fnam{idx(e)} '=' num2str(val(1))];
    if idx(e) == 13
      cmd = [cmd ',' num2str(val(2)) largesize];
    end
  end
  delim = '&';
end

% Send ptz command to camera
if ~isempty(idx)
  urlread(cmd);
%  display(cmd)
  pause(4); % Wait for camera to move to new settings
end

% Read image from camera
im = imread([camadr largejpg]);
