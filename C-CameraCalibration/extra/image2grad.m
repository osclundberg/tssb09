function dIm = image2grad(Im,s,fsz,a,th,rmborder)

% dIm = image2grad(Im,s,fsz,a,th,rmborder)
%
% Computes the image gradient, dIm, using a differentiated Gaussian.
% OBS! The gradient is computed in Matlabs own coordinate system
%      (x1,x2), not the Cartesian (x,y).
%
%  Im - M/N matrix
%   s - standard deviation of the Gauss
% fsz - filter size
%       (default a function of s)
%   a - Remapping of the magnitude, |dIm| -> |dIm|^a
%       (default a=1)
%  th - energy threshold
%       (default th=0)
% rmborder - If 1 then remove set results near border to zero
%       (default 0)
%
% dIm - M/N/2 matrix
%
%
%
% bjojo 2001-01-13

if nargin<3 | isempty(fsz), fsz = 1+2*ceil(s*sqrt(2*log(100)));end
if nargin<4 | isempty(a), a = [];end
if nargin<5 | isempty(th), th = [];end
if nargin<6 | isempty(rmborder), rmborder = 0;end
Im = double(Im);

% Create filters
%----------------

x = (1:fsz)-0.5-fsz/2;
g = exp(-0.5*(x/s).^2)/sqrt(2*pi)/s;

xg = x.*g;

% Convolve
%----------

dIm = zeros(size(Im,1),size(Im,2),2);
dIm(:,:,1) = imfilter(imfilter(Im,g),-xg');
dIm(:,:,2) = imfilter(imfilter(Im,-xg),g');

if ~isempty(a) & a~=1
  nrmdIm = sum(dIm.^2,3)+eps;
  dIm(:,:,1) = dIm(:,:,1).*nrmdIm.^((a-1)/2);
  dIm(:,:,2) = dIm(:,:,2).*nrmdIm.^((a-1)/2);
end

if ~isempty(th) & th~=0
  nrmdIm = sum(dIm.^2,3)+eps;
  mx = max(nrmdIm(:));
  dIm(:,:,1) = dIm(:,:,1).*(nrmdIm>th^2*mx);
  dIm(:,:,2) = dIm(:,:,2).*(nrmdIm>th^2*mx);
end

if rmborder~=0
  d = ceil(2*s);
  dIm([1:d end-d+1:end],:,:) = 0;
  dIm(:,[1:d end-d+1:end],:) = 0;
end
