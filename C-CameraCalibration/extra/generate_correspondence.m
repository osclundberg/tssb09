%function [Xi,Yi,Xw,Yw]=generate_correspondence(X,Y,origin,xdirn,ydirn,dmax)
%
% Generate corresponding lists of world coordinates
% to the image coordinates in X,Y
%
% DMAX   Maximum allowed distance in image 
%        relative to grid positions (default 0.12)
%
% Per-Erik Forssen, March 2003
% [Mod by ErikJ, 2006]
function [Xi,Yi,Xw,Yw]=generate_correspondence(X,Y,origin,xdirn,ydirn,dmax)

% Indroduce variables - Modified by Maja 2011-11-02
Xi = 0;  Yi = 0;  Xw = 0;  Yw = 0;

if nargin<6,dmax=0.12;end

% Allocate output arrays
N=size(X,1);
Opos=zeros(N,4);                  % Xi Yi Xw Yw (out arrays)

% Step lengths in X and Y directions
dx=sqrt(xdirn'*xdirn);
dy=sqrt(ydirn'*ydirn);
maxd=sqrt(dx^2+dy^2)*dmax;        % Maximum allowed distance

visit=zeros(N,1);                 % Markers indicating visited elements
Tpos=zeros(N,8);                  % Xi Yi Xw Yw dx1 dx2 dy1 dy2 (queue)
lc=0;                             % Number of elements in list
% Try to add origin
[mv,mind]=closest_point(origin,X,Y);
if(mv<maxd),
  lc=lc+1;
  visit(mind)=1;                  % Mark as visited
  Tpos(lc,:)=[X(mind) Y(mind) 0 0 xdirn' ydirn'];
else
  % Modified by Maja 2011-11-02
  %  error('Something is wrong!');
  disp('Something is wrong!');
  return;
end

% Aha! N�t med normeringen!

rind=1;   % Insert pos in out list

while(lc>0)
  % Consume top of stack
  Cpars=Tpos(lc,:);
  Cpos=Cpars(1:4);
  Cdx=Cpars(5:6);
  Cdy=Cpars(7:8);
  lc=lc-1;
  % Store in out list
  Opos(rind,:)=Cpos;
  rind=rind+1;
  
  % Try to add each of the four neighbours
  Qpos=zeros(4,4);
  Qpos(1,:)=Cpos+[-Cdx -10   0];  % Left
  Qpos(2,:)=Cpos+[+Cdx +10   0];  % Right
  Qpos(3,:)=Cpos+[-Cdy   0 -10];  % Up
  Qpos(4,:)=Cpos+[+Cdy   0 +10];  % Down
  
  for k=1:4,
    [mv,mind]=closest_point(Qpos(k,1:2),X,Y);
    if (mv<maxd) && (visit(mind)==0)
      lc=lc+1;
      Npos=[X(mind) Y(mind)];
      % Vector between old and new pos
      Ndq=Npos-Cpos(1:2);
      
      % Update the step vectors depending on in which direction we are
      % looking (steps are always related to the original axis) 
      % [BUGFIX by Erik, 2006: Previously, we could give the same world
      % coord to several image points]
      Ndx = Cdx;
      Ndy = Cdy;
      switch k
        case 1, Ndx = -Ndq; 
        case 2, Ndx = Ndq; 
        case 3, Ndy = -Ndq; 
        case 4, Ndy = Ndq; 
      end
      Tpos(lc,:)=[Npos Qpos(k,3:4) Ndx Ndy];
      visit(mind)=1;
    end
  end
end

% Crop out array
rind=rind-1;
Opos=Opos(1:rind,:);

Xi=Opos(:,1);
Yi=Opos(:,2);
Xw=Opos(:,3);
Yw=Opos(:,4);

%
% Subroutine to find closest point
% Cpos=[Xc Yc]
%
function [r,ind]=closest_point(Cpos,X,Y)
R=sqrt((X-Cpos(1)).^2+(Y-Cpos(2)).^2);
[r,ind]=min(R);
