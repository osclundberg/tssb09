figure(1)
objectImage = imread('img/object.jpg');
coordImage = imread('img/coordinates.jpg');

%%imagesc(objectImage); axis image;
%%figure 
%%imagesc(coordImage); axis image;

%%

calibr;

c = pinv(D)*f;
c =[c; 1];
C =  (reshape(c, 3, 3))';

test = C * [5 5 1]'
u5new = test(1) / test(3)
v5new = test(2) / test(3)

invC = inv(C);

a = [113 145 1]';
b = [209 162 1]';

XYa = invC * a;
XYb = invC * b;

sa = 1/XYa(3);
sb = 1/XYb(3);

XYa = sa * invC * a; 
XYb = sb * invC * b;


diffXY = XYb - XYa;

length = sqrt(diffXY(1, 1)^2 + diffXY(2, 1)^2)



%% 2.4 

A = 1.0e+003 * [ 1.2872 -0.0020 0.1898; 
  0 1.1672 0.1340;
  0 0 0.0010];

R = [ 0.8498 0.5255 -0.0147;
     -0.5269 0.8505 -0.0034;
      0.0107  0.0106 0.9997];

t= [-17.4148 -12.8092 853.6756];


%% 2.5 To follow an object with a network camera

A = 1000 * [1.3015 -0.0015 0.1889; 0 1.1803 0.1402; 0 0 0.0010];
v = [0 0 1]';

vNormalized = inv(A) * v;

thetaU = atan( vNormalized(1) );
thetaV = atan( vNormalized(2) );

thetaU = radtodeg(thetaU)
thetaV = radtodeg(thetaV)



%% 2.6 From C to A, R, t

A = 1.0e+003 * [ 1.2872 -0.0020 0.1898; 
  0 1.1672 0.1340;
  0 0 0.0010];

R = [ 0.8498 0.5255 -0.0147;
     -0.5269 0.8505 -0.0034;
      0.0107  0.0106 0.9997];

t= [-17.4148 -12.8092 853.6756];

C = A * [R t'];

[K R t] = P2KRt(C)

