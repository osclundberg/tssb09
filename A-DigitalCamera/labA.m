% Setup
addpath shadingcorr
addpath bayer 

load darkimage.mat
load blackimage.mat
load origimage.mat
load whiteimage.mat



%% Part 2.1: Shading correction

fa = mean(mean(whiteimage));
fb = mean(mean(blackimage));

bA = whiteimage;
bB = blackimage;

[imageRows, imageColumns] = size(origimage);

C = zeros(imageRows, imageColumns);
D = zeros(imageRows, imageColumns);

correctedImage = zeros(imageRows, imageColumns);

% Calculation of correction values and corrected image

for i = 1:imageRows
    for j = 1:imageColumns
        % do magic
        C(i,j) = (fb - fa) / (bB(i,j) - bA(i,j));
        D(i,j) = ( fa * (bB(i,j) - bA(i,j)) ) / (fb - fa)  - bA(i,j); 
        correctedImage(i,j) = C(i,j) * ( origimage(i,j) + D(i,j));
    end
end


figure(1);
imshow(origimage, [0 255]);
colormap(gray);

figure(2);
imshow(correctedImage, [0 255]);
colormap(gray);

%% Part 2.2: Deinterlacing

im = im2double(imread('interlaced_image.png'));
imshow(im);




%%

[rows,cols,ndim]=size(im);
mask1=zeros(rows,cols);
mask1(1:2:end,:)=1;
mask2=zeros(rows,cols);
mask2(2:2:end,:)=1;

figure;

subplot(1,2,1), imagesc((im.*repmat(mask1, [1 1 3])), [0 255]); axis image;
subplot(1,2,2), imagesc((im.*repmat(mask2, [1 1 3])), [0 255]); axis image;

%imshow(im.*repmat(mask1, [1 1 3]));
%imshow(im.*repmat(mask2, [1 1 3]));

% conv2 
f = [1 2 1]' / 2;

%im1 = zeros(size(im));
%im2 = zeros(size(im));

for k=1:3,
    bk = im(:,:,k);
    im1(:,:,k) = conv2((bk.*mask1),f);
    im2(:,:,k) = conv2((bk.*mask2),f);
end


figure;
subplot(1,2,1), imagesc(im1, [0 255]); axis image;
subplot(1,2,2), imagesc(im2, [0 255]); axis image;


%%

% Part 2.3: Bayer pattern interpolation
clear all;
clc;

im = im2double(imread('bayer/raw0001.png'));
% BGGR bayer pattern
[rows,cols,ndim]=size(im);
mask1=zeros(rows,cols);
mask1(2:2:end, 2:2:end) = 1;
mask2=zeros(rows,cols);
mask2(1:2:end, 2:2:end) = 1;
mask2(2:2:end, 1:2:end) = 1;
mask3=zeros(rows,cols);
mask3(1:2:end, 1:2:end) = 1;

f = [1 2 1]/4;

img = conv2(f, f, im.*mask1, 'same') ./ conv2(f,f,mask1, 'same');
%img = conv2(f, f, im.*mask1, 'same') ./ conv2(f,f,mask1, 'same');

mean(mean(im))
mean(mean(img))

%%

im_rgb=reshape([im.*mask1 im.*mask2 im.*mask3],[size(im) 3]);
imshow(im_rgb);

im = raw2rgb(im);
figure;
imshow(im);

%% Part 2.4: Noise measurements

fpath='bayer/';
im=cell(100,1);
for k=1:100,
    fname=sprintf('raw%04d.png',k);
    im{k}=imread([fpath fname]);
end

for k=1:100,
    %im{k} = raw2rgb(im{k});
    %imshow(im{k});
    %axis([871 980 401 480]);
    %drawnow

end

[rows, cols, ndim] = size(im{1});
imm = zeros(rows, cols, ndim);

%%
% calculate average
for k=1:100 
    imm = imm + im2double(im{k});
end
imm = imm / 100;

imCompare = raw2rgb(im2double(imread('bayer/raw0001.png')));


% Plot average against a comparable
subplot(1,2,1), imagesc(imm, [0 255]); axis([871 980 401 480]);
title('Average');
subplot(1,2,2), imagesc(imCompare, [0 255]); axis([871 980 401 480]);
title('Compare');

%% Variance

imv = zeros(size(im{1}));


for k=1:100
    imv = imv + im2double(im{k}).^2;
end

imv = imv / 100 - imm .* imm;

figure(3); 
imgvrgb = raw2rgb(imv);

%imgvrgb = imgvrgb / max(max(max(imgvrgb)));

imshow(imgvrgb);
figure
imshow(imv);

%% Signal to Noise Ratio
immrgb = raw2rgb(imm);

snr(immrgb, imgvrgb) % 80.1608 dB

snrimg = sqrt(imgvrgb ./ immrgb.^2);

snrimg = snrimg / max(max(max(snrimg))); %normalize

imshow(snrimg);


%% Plot noise variance 

indim=uint8(imm*255);
rhist=zeros(256,1);
ghist=zeros(256,1);
bhist=zeros(256,1);

%Red
mask1=zeros(rows,cols);
mask1(2:2:end, 2:2:end) = 1;
%green
mask2=zeros(rows,cols);
mask2(1:2:end, 2:2:end) = 1;
mask2(2:2:end, 1:2:end) = 1;
%Blue
mask3=zeros(rows,cols);
mask3(1:2:end, 1:2:end) = 1;

for k=0:255,
    k;
    redk=find(mask1);
    indk=redk(find(indim(redk)==k));
    rhist(k+1)=sum(imv(indk))/(length(indk)+eps);
    greenk=find(mask2);
    indk=greenk(find(indim(greenk)==k));
    ghist(k+1)=sum(imv(indk))/(length(indk)+eps);
    bluek=find(mask3);
    indk=bluek(find(indim(bluek)==k));
    bhist(k+1)=sum(imv(indk))/(length(indk)+eps);
end


plot([0:255],[bhist ghist rhist]*255^2)
axis([0 255 0 10])
grid on


