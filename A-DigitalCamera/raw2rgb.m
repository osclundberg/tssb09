function [ rgb ] = raw2rgb( im, filter )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here


im = im2double(im);

[rows,cols,ndim]=size(im);



% Bayer pattern BGGR
if ~exist('filter', 'var')
   filter = 'BGGR';
end


if strcmp(filter,'GBRG')
    
    mask1=zeros(rows,cols);
    mask1(2:2:end, 1:2:end) = 1;
    mask2=zeros(rows,cols);
    mask2(1:2:end, 1:2:end) = 1;
    mask2(2:2:end, 2:2:end) = 1;
    mask3=zeros(rows,cols);
    mask3(1:2:end, 2:2:end) = 1;
    
end
if strcmp(filter,'GRBG')
    
    mask1=zeros(rows,cols);
    mask1(1:2:end, 2:2:end) = 1;
    mask2=zeros(rows,cols);
    mask2(1:2:end, 1:2:end) = 1;
    mask2(2:2:end, 2:2:end) = 1;
    mask3=zeros(rows,cols);
    mask3(2:2:end, 1:2:end) = 1;
    
end
if strcmp(filter,'BGGR')
    %Red
    mask1=zeros(rows,cols);
    mask1(2:2:end, 2:2:end) = 1;
    %green
    mask2=zeros(rows,cols);
    mask2(1:2:end, 2:2:end) = 1;
    mask2(2:2:end, 1:2:end) = 1;
    %Blue
    mask3=zeros(rows,cols);
    mask3(1:2:end, 1:2:end) = 1;
    
end
if strcmp(filter,'RGGB')
    
    mask1=zeros(rows,cols);
    mask1(1:2:end, 1:2:end) = 1;
    mask2=zeros(rows,cols);
    mask2(1:2:end, 2:2:end) = 1;
    mask2(2:2:end, 1:2:end) = 1;
    mask3=zeros(rows,cols);
    mask3(2:2:end, 2:2:end) = 1;
    
end



% Normalized averaging / non-linear mean filtering
f=[1 2 1]/4;

%img = zeros(rows, cols, ndim);

red = conv2(f,f,im.*mask1,'same') ./ conv2(f,f,mask1,'same'); % Red
green = conv2(f,f,im.*mask2,'same') ./ conv2(f,f,mask2,'same'); % Green
blue = conv2(f,f,im.*mask3,'same') ./ conv2(f,f,mask3,'same'); % Blue

img = reshape([red green blue], [size(im) 3]);

rgb = img;

end

