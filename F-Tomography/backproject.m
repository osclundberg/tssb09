function [f, M] = backproject(q, rVec, phiVec, xVec, yVec, intpol)

[xGrid, yGrid] = meshgrid(xVec, yVec);
f = zeros(size(xGrid));
Nr = length(rVec);
Nphi = length(phiVec);
deltaR = rVec(2) - rVec(1);


switch intpol
  case 'nearest'
    for phiIx = 1:Nphi
      phi = phiVec(phiIx);
      proj = q(:, phiIx); 
      r = xGrid * cos(phi) + yGrid * sin(phi);
      rIx = round(r / deltaR + (Nr + 1) / 2); %index rounded
      f = f + proj(rIx);
      subplot(2,2,4); imagesc(yVec, xVec, f);
      caxis([1 1.04]); axis image;
      M(phiIx) = getframe;
    end
  case 'linear'
    % your code here
    for phiIx = 1:Nphi
      phi = phiVec(phiIx);
      proj = q(:, phiIx); 
      r = xGrid * cos(phi) + yGrid * sin(phi); %
      rIx = (r / deltaR + (Nr + 1) / 2);
      
      rIx_floor = floor(rIx);
      rIx_ceil = ceil(rIx);
      
      d = rIx;
      y0 = proj(rIx_floor);
      y1 = proj(rIx_ceil);
      x0 = rIx_floor;
      x1 = rIx_ceil;
      
      f = f + y0 + (y1-y0).*(d - x0); 
      
      % Take snapshot for movie
      subplot(2,2,4); imagesc(yVec, xVec, f);
      axis xy; axis image; colorbar;
      M(phiIx) = getframe;
    end
  otherwise
    error('Unknown interpolation.');
end

f = f * pi / (Nphi*deltaR);
