function p = generateProj2(E, rVec, phiVec, oversampling)

Nr = length(rVec) * oversampling;
rLen = (rVec(2) - rVec(1)) / oversampling * Nr;
rVec = (0.5 : Nr - 0.5) * rLen / Nr - rLen / 2;

x0 = E(:, 1);          % x offset
y0 = E(:, 2);          % y offset
a = E(:, 3);           % radius a
b = E(:,4);            % radius b
alpha = deg2rad(E(:,5));        % alpha
dens = E(:, 6);        % realistic density
hcd = E(:,7);          % high contrast density
%dens = hcd;

p = zeros(length(rVec), length(phiVec));

for phiIx = 1:length(phiVec)
  phi = phiVec(phiIx);
  for i = 1:length(x0)
    aSqr = a(i).^2;
    bSqr = b(i).^2;
    
    r0Sqr = aSqr*(cos(phi - alpha(i))).^2;
    r0Sqr = r0Sqr + bSqr*(sin(phi-alpha(i))).^2;
    
    r1Sqr = (rVec' - x0(i) * cos(phi) - y0(i) * sin(phi)).^2;
    
    ind = find(r0Sqr > r1Sqr);
    
    temp = 2*a(i)*b(i)*dens(i)*( sqrt(r0Sqr - r1Sqr(ind)) / r0Sqr);
       
    p(ind, phiIx) = p(ind, phiIx) + temp;
  end
end

p = conv2(p, ones(oversampling, 1) / oversampling, 'same');
p = p(round(oversampling/2):oversampling:end, :);
