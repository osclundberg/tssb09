function phm = pixelize2(E, xVec, yVec);

[xGrid, yGrid] = meshgrid(xVec, yVec);
phm = zeros(size(xGrid));

for i = 1 : size(E, 1)        
   x0 = E(i, 1);          % x offset
   y0 = E(i, 2);          % y offset
   a = E(i, 3);           % radius a
   b = E(i,4);            % radius b
   alpha = E(i,5);        % alpha
   dens = E(i, 6);        % realistic density
   hcd = E(i,7);          % high contrast density
   
   %dens= hcd;
   
   alpha = deg2rad(alpha);

   
   x = xGrid - x0;
   y = yGrid - y0;
   idx = find( ((x*cos(alpha) + y*sin(alpha))/a).^2 + ((x*sin(alpha) - y*cos(alpha))/b).^2 < 1 );
   
   
   phm(idx) = phm(idx) + dens;
end

