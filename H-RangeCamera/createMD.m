function [ R1 ] = createMD( rangeim )
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here

tick_divide = 4.2;
r = rangeim.range;
y = rangeim.x;
x = repmat(double(rangeim.prof_id)/tick_divide,[size(y,1) 1]);
imsz = size(r);
R1 = zeros([imsz 3]);
R1(:,:,1) = double(y);
R1(:,:,2) = double(x);
R1(:,:,3) = double(r);
cert = (r == 0);
R1c = repmat(cert,[1 1 3]);
R1(R1c) = NaN;

end

