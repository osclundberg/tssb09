% Introduction
fpath = '';
fp = fopen([fpath 'handran'],'r');
a = fscanf(fp,'%d',[500,256]); % image size: 500*256
figure(1)
imagesc(a, [0 100]);
axis image; colorbar;
colormap([(0:255)'/255,(0:255)'/255,(0:255)'/255])

fp = fopen([fpath 'handint'],'r');
b = fscanf(fp,'%d',[500,256]); % image size: 500*256
figure(2)
imagesc(b);
axis image; colorbar;
colormap([(0:255)'/255,(0:255)'/255,(0:255)'/255])

figure(3);
T = 35;
c = a.*(b>T);
imagesc(c);
axis image; colorbar;
rainbow = jet;
rainbow(1,:) = [0.5 0.5 0.5]; 
colormap(rainbow);


%%
figure(4);
fp=fopen([fpath 'musran'],'r');
b = fscanf(fp,'%d',[250,256]); % image size: 250*256
imagesc(b);
axis image; colorbar;
colormap(rainbow)


figure(5);
fp = fopen([fpath 'wood'],'r');
g = fscanf(fp,'%d',[512,512]); % image size: 512*512
imagesc(g);
axis image; colorbar;
colormap(rainbow)


figure(6);
dim =1;
[y i] = max(g, [], dim);

plot(i);
figure(7);
plot(y);

%% 5 Calibration

calibration


%%
load('trapez.mat');
load('notrapez.mat')

figure(8);
imshow(trapez, []);
figure(9);
imshow(notrapez, []);

p1 = [72 175 1]'; 
p2 = [134 123 1]';
p3 = [390 139 1]';
p4 = [473 205 1]';

% Convert to world coordinates
p1 = inv(C) * p1;
p2 = inv(C) * p2;
p3 = inv(C) * p3;
p4 = inv(C) * p4;

% normalize
p1 = p1 / p1(3);
p2 = p2 / p2(3);
p3 = p3 / p3(3);
p4 = p4 / p4(3);

l1 = pdist([p1';p2'],'euclidean');
l2 = pdist([p2';p3'],'euclidean');
l3 = pdist([p3';p4'],'euclidean');
l4 = pdist([p4';p1'],'euclidean');

circ_estimated = l1 + l2 + l3 + l4
circ_real = 340;

error_marg = circ_real / circ_estimated 

%% Measuring tiny details

load('letter.mat');

R1 = createMD(rangeim);

%{
tick_divide = 5;
r = rangeim.range;
y = rangeim.x;
x = repmat(double(rangeim.prof_id)/tick_divide,[size(y,1) 1]);
imsz = size(r);
R1 = zeros([imsz 3]);
R1(:,:,1) = double(y);
R1(:,:,2) = double(x);
R1(:,:,3) = double(r);
cert = (r == 0);
R1c = repmat(cert,[1 1 3]);
R1(R1c) = NaN;
%}

mesh(R1(:,:,1), R1(:,:,2), R1(:,:,3)); axis equal;

%% 6.6 Volume measurement of object

load('vit_vaggkontakt.mat');

R1 = createMD(rangeim);

R2 = R1(401:700,201:700,:);
Intens = rangeim.intens(401:700,201:700);
figure(1)
meshc(R2(:,:,1),R2(:,:,2),R2(:,:,3))
figure(2)
subplot(2,2,1), imagesc(R2(:,:,1)), title('y image');
subplot(2,2,2), imagesc(R2(:,:,2)), title('x image');
subplot(2,2,3), imagesc(R2(:,:,3)), title('r image');
subplot(2,2,4), imagesc(Intens(:,:)), title('i image');
figure(3)
subplot(2,2,1), plot(R2(:,320,1)), title('vert, y');
subplot(2,2,2), plot(R2(150,:,2)), title('horiz, x');
subplot(2,2,3), plot(R2(:,320,3)), title('vert, r');
subplot(2,2,4), plot(R2(150,:,3)), title('horiz, r');



%mesh(R1(:,:,1), R1(:,:,2), R1(:,:,3)); axis equal;


%% Riemann integral 

% Real house object volume (m?ns version): 79 cm3
% Real house object volume (den r?tta versionen): 64 cm3

load('hus0.mat');
%load('hus90.mat');
%load('hus_lutande0.mat');
%load('hus_lutande90.mat');

R1 = createMD(rangeim);

R2 = R1(401:700,201:700,:);
Intens = rangeim.intens(401:700,201:700);
figure(1)
meshc(R2(:,:,1),R2(:,:,2),R2(:,:,3))
figure(2)
subplot(2,2,1), imagesc(R2(:,:,1)), title('y image');
subplot(2,2,2), imagesc(R2(:,:,2)), title('x image');
subplot(2,2,3), imagesc(R2(:,:,3)), title('r image');
subplot(2,2,4), imagesc(Intens(:,:)), title('i image');
figure(3)
subplot(2,2,1), plot(R2(:,320,1)), title('vert, y');
subplot(2,2,2), plot(R2(150,:,2)), title('horiz, x');
subplot(2,2,3), plot(R2(:,320,3)), title('vert, r');
subplot(2,2,4), plot(R2(150,:,3)), title('horiz, r');

% Components needed for volume
dx = 1 / 4.2;
groundlevel = 401.5;
dy = [zeros(1,500); diff(R2(:,:,1))];
 
% calculate volume in each pixel
volume_mat = dy(:,:) .* (R2(:,:,3) - groundlevel) .* dx .* (R2(:,:,3) > 410);

% Riemann sum over all pixel -> volume of object
volume_estimated = nansum(nansum(volume_mat))



