% Read a CT-volume.
% 2009-11 Maria Magnusson
%------------------------------------

% user must load volum with the following command
%------------------------------------------------
load ctvol.mat;

% check sizes
%------------
siz = size(ctvol);
sizex = siz(1);
sizey = siz(2);
sizez = siz(3);
zmax = 150;

% open figure with gray colormap
%-------------------------------
figure(3);
colormap gray;

% Plot 3 slices from image volume
%--------------------------------
subplot(2,2,1), imshow(ctvol(:,:,1), [0 max(max(ctvol(:,:,1)))]);
title('slice 1'); axis image; colorbar;
subplot(2,2,2); imshow(ctvol(:,:,5), [0 max(max(ctvol(:,:,5)))]);
title('slice 5'); axis image; colorbar;
subplot(2,2,3); imshow(ctvol(:,:,10), [0 max(max(ctvol(:,:,10)))]);
title('slice 10'); axis image; colorbar;
