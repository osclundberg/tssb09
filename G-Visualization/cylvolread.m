% Read an image volume.
% Make a depth-coded image.
% 2009-11 Maria Magnusson
%------------------------

% load volume
%------------
load cylvol.mat;

cylvol = ctvol(:,:,:);

cylvol = flip(cylvol, 3);
cylvol = rot90(cylvol,-1);
cylvol = imrotate(cylvol,-4, 'bicubic');
%cylvol = imrotate(cylvol, -4);
% check sizes, note that (rol,col,z) corresponds to (y,x,z)
%----------------------------------------------------------
siz = size(cylvol);
sizex = siz(2);
sizey = siz(1);
sizez = siz(3);
%zmax = sizez;
zmax = sizez;

% open figure with gray colormap
%-------------------------------
figure(2);
colormap gray;

% Plot 3 slices from image volume
%--------------------------------


subplot(2,2,1), imshow(cylvol(:,:,5), [0 255]);
title('slice 5'); axis image; colorbar;
subplot(2,2,2), imshow(cylvol(:,:,15), [0 255]);
title('slice 15'); axis image; colorbar;
subplot(2,2,3), imshow(cylvol(:,:,32), [0 255]);
title('slice 32'); axis image; colorbar;

% Make a depth-image in the y-direction 
%--------------------------------------
depth = zeros(sizez,sizex);
thresh = 1300;
for z = 1 : zmax,
  for x = 1 : sizex,
    y=1;
    while (y<sizey) & (cylvol(y,x,z)<thresh),
      y=y+1;
    end;
    if cylvol(y,x,z) >= thresh
      depth(z,x)=sizey-y;
    end;    
  end;
end;




% Create sobel filters
sobelX = zeros(3,3,3);
sobelX(:,:,1) = [-1 0 1 ; -2 0 2; -1 0 1];
sobelX(:, :, 2) = [-2 0 2 ; -4 0 4 ; -2 0 2];
sobelX(:,:,3) = [-1 0 1 ; -2 0 2 ; -1 0 1];
sobelX = sobelX ./ 32;

sobelY = zeros(3,3,3);
sobelY(:,:,1) = [-1 -2 -1 ; 0 0 0; 1 2 1];
sobelY(:, :, 2) = [-2 -4 -2 ; 0 0 0 ; 2 4 2];
sobelY(:,:,3) = [-1 -2 -1 ; 0 0 0 ; 1 2 1];
sobelY = sobelY ./ 32;

sobelZ = zeros(3,3,3);
sobelZ(:,:,1) = [-1 -2 -1 ; -2 -4 -2; -1 -2 -1];
sobelZ(:, :, 2) = [0 0 0 ; 0 0 0 ; 0 0 0];
sobelZ(:,:,3) = [1 2 1 ; 2 4 2; 1 2 1];
sobelZ = sobelZ ./ 32;

% Normals holder
C = cell(sizez,sizex);

indicies = zeros(64,64);



for z = 1 : zmax,
  for x = 1 : sizex,
    y=1;
    while (y<sizey) & (cylvol(y,x,z)<thresh),
      y=y+1;
    end;
    C{z,x} = 0;
    if z > zmax-1 | y > sizey-1 | x > sizex-1 | x < 2 | y < 2 | z < 2  
    elseif cylvol(y,x,z) >= thresh 
        %indicies(z,x) = y;
               
        piece = cylvol(y-1:y+1,x-1:x+1,z-1:z+1);
        piece = cast(piece,'double');
     
        dx = sum(sum(sum(piece.*sobelX)));
        dy = sum(sum(sum(piece.*sobelY)));
        dz = sum(sum(sum(piece.*sobelZ)));
        
        norm = sqrt(dx^2+dy^2+dz^2);
        normal = [dx dy dz] ./ norm;
        
        C{z,x} = normal;
        
        
    end;    
  end;
end;

img = zeros(sizez,sizex);

kd = 0.8;
ka = 0.5;
ks = 0.4;
intensity = 255;
Lm = [0 1 0];
V = [0 1 0];
alpha = 0.1;



for i = 1 : sizez
   for j = 1 :sizex
       
       if (C{i,j} == 0) 
       else
           % Calucate reflection vector
           R = - Lm + 2 * dot(Lm, C{i,j}) * C{i,j};
           if dot(C{i,j}, Lm) > 0
            img(i,j) =  kd * dot(C{i,j},Lm) * intensity; % diffuse
           end
           if dot(V,R) > 0
            img(i,j) = img(i,j) + ks * intensity * (dot(V,R) .^6 ); % specular
           end
       end
      
   end
end

img = img / max(max(max(img))); % normalize
subplot(2,2,4); imshow(img, []);
title('Phong'); axis image; colorbar;


%% Generate 3D image.
figure(3);
image_3d = zeros(230, 345,3);
image_3d(:,:,2) = 0.5*left;
image_3d(:,:,3) = 0.5*left;
image_3d(:,:,1) = right;
figure;
imshow(image_3d, [0 255]);
