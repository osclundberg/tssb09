% Setup

load Sensor1/Refdata1
load Sensor1/Refdata2
load Sensor1/Refdata3
load Sensor1/Scenedata

% A. Plotting the sensor data

timeInstances = 20;

% A-1
for k=1:timeInstances
    imshow(Refdata1(:,:,k), []);
    drawnow;
end

% A-2 & A-3

radiances = [0.809 1.58 2.25]; % 2.5, 22, 34 (Celcius)

averages = zeros(3,1);
good = zeros(3,1);
bad = zeros(3,1);

averages(1) = mean(mean(Refdata1(:,:,1)));
averages(2) = mean(mean(Refdata2(:,:,1)));
averages(3) = mean(mean(Refdata3(:,:,1)));

bad(1)=Refdata1(13,11,1);
bad(2)=Refdata2(13,11,1);
bad(3)=Refdata3(13,11,1);

good(1)=Refdata1(178,104,1);
good(2)=Refdata2(178,104,1);
good(3)=Refdata3(178,104,1);

%%

badArr = zeros(timeInstances, 1); 

for i=1:timeInstances
    averages(1) = mean(mean(Refdata1(:,:,i)));
    averages(2) = mean(mean(Refdata2(:,:,i)));
    averages(3) = mean(mean(Refdata3(:,:,i)));

    bad(1)=Refdata1(13,11,i);
    bad(2)=Refdata2(13,11,i);
    bad(3)=Refdata3(13,11,i);

    badArr(i) = bad(1);
    
    good(1)=Refdata1(178,104,i);
    good(2)=Refdata2(178,104,i);
    good(3)=Refdata3(178,104,i);
    
    plot(i, bad(1) );
    hold on;
    
end




%%

averages = averages';

plot( averages, radiances, 'x' )
hold on
plot(bad, radiances,'o')
hold on
plot(good, radiances,'*')
hold on

%polyfit(averages, 1);

%plot(x, x * 8.53 * 10^-5 - 4.6554)

% B-1

imagesc(Scenedata)






