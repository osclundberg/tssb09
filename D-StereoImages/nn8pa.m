% script n8pa
%
% Implements the 8-point algorothm
%
% Assumes that y1 and y2 are 3xN matrices which hold corresponding
% homogeneous image coordinates in their colums, properly normalized so
% that third element = 1.

A=[];
for ix=1:size(y1,2),
  A=[reshape(y2(:,ix)*y1(:,ix)',1,9); A];
end

[U S V]=svd(A);
fprintf('Singular values of data matrix:\n');
diag(S)'

F_vec=V(:,end);

F=reshape(F_vec,3,3);

[Uf Sf Vf]=svd(F);
S0=Sf;
S0(3,3)=0;
F0=Uf*S0*Vf';

F=F0;
