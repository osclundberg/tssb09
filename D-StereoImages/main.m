% Lab D Image Rectification

im1=imread('dinosaur0.png');
im2=imread('dinosaur5.png');
load('dino_Ps','P');
figure(1);imagesc(im1);
figure(2);imagesc(im2);
C1=P{1};       % The camera matrix corresponding to image 1
C2=P{6};       % The camera matrix corresponding to image 2


% null space contains the epipoles in WCS positions
n1 = null(C1);
n2 = null(C2);

% Theoretical epipoles (calibrated case)
e21 = C2 * n1; 
e12 = C1 * n2;

e21 = e21/e21(3); % normalize
e12 = e12/e12(3); % normalize

% Fundamental matrices 
F21 = liu_crossop(e21) * C2 * C1' * inv(C1 * C1');
F12 = liu_crossop(e12) * C1 * C2' * inv(C2 * C2');

%% 4.1 Introduction

% Pick correspondence points in the images
% structure: y1 = [x1 y1; x2 y2; ... ]' where x,y positions in image 1 

%y1 = [420 453; 438 285; 86 319; 253 176; 295 100]';
%y2 = [349 469; 368 307; 93 210; 260 139; 302 81]';

y1 = [ 420 452; 398 367; 439 285; 410 153; 416 86; 409 30; 295 103; 237 232; 94 312; 204 426 ]';
y2 = [ 348 468; 440 394; 370 306; 485 190; 423 111; 406 52; 302 83; 245 189; 101 205; 190 354 ]';

% convert to homogeneous coordinates
y1 = vgg_get_homg(y1);
y2 = vgg_get_homg(y2);

% Calculate epipolar lines from correspondence points
l2=F21*y1;
l1=F21'*y2;
for ix=1:size(y1,2),
  l1(:,ix)=-l1(:,ix)/norm(l1(1:2,ix))*sign(l1(3,ix));
  l2(:,ix)=-l2(:,ix)/norm(l2(1:2,ix))*sign(l2(3,ix));
end

% Plot epipoles and epipolar lines

for ix=1:size(y1,2),
  figure(1);hold('on');plot(y1(1,ix),y1(2,ix),'or');hold('off');
  figure(1);hold('on');drawline(l1(:,ix));hold('off');
  figure(2);hold('on');plot(y2(1,ix),y2(2,ix),'or');hold('off');
  figure(2);hold('on');drawline(l2(:,ix));hold('off');
end

%%
% Calculate distances

abs(sum(y1.*l1))
% ans:  1.2129    0.8295    0.7614    0.4443    0.1673
abs(sum(y2.*l2))
% ans: 1.2235    0.8421    0.7062    0.4328    0.1650

% The commands above describes 
% how far from the epipolar line the corresponding points are (in pixel
% distance)
% Our measurements are within the distance expected. 1-2 pixels.
% We choose within 1-3 pixels.

% Calculate epipoles empirically
% ??????????????
epi21 = null(F12);
epi12 = null(F21);

epi21 = epi21/epi21(3); % normalize
epi12 = epi12/epi12(3); % normalize

%% 4.2 The loop & Zhang method

%Distortion


w=720;h=576; % Image width and height
lambda = -5:0.1:5; %Replace with the interval you choose
dist = LoopZhangDistortion(e12,e21,w,h,lambda);
figure(1);plot(lambda,dist);
figure(2);plot(lambda(2:end),diff(dist));
minlamda = lambda(dist==min(dist)) %Print minimum lambda

% minlamda = -1.30000

lambda = minlamda;    % Insert your value here
w1=liu_crossop(e12)*[lambda 1 0]';
w1=w1/w1(3);
w2=F21*[lambda 1 0]';
w2=w2/w2(3);
Hp1=[1 0 0;0 1 0;w1'];
Hp2=[1 0 0;0 1 0;w2'];

vcp=0;
Hr1=[F21(3,2)-w1(2)*F21(3,3) w1(1)*F21(3,3)-F21(3,1) 0;
     F21(3,1)-w1(1)*F21(3,3) F21(3,2)-w1(2)*F21(3,3) F21(3,3)+vcp;
     0 0 1];
Hr2=[w2(2)*F21(3,3)-F21(2,3) F21(1,3)-w2(1)*F21(3,3) 0;
     w2(1)*F21(3,3)-F21(1,3) w2(2)*F21(3,3)-F21(2,3) vcp;
     0 0 1];

H1 = Hr1 * Hp1;
H2 = Hr2 * Hp2;

% Flip images
flipMatrix = eye(3);%[1 0 0; 0 -1 0; 0 0 1];

H1 = flipMatrix * H1; 
H2 = flipMatrix * H2;

% Verification of H2 and H1
ix = [0 0 0; 0 0 -1; 0 1 0];
H2' * ix * H1; % == F21


%% 4.3 Rectifying the images

oldcorners=[1 w w 1;1 1 h h];
newcorners1=map_points(H1,oldcorners);
newcorners2=map_points(H2,oldcorners);
mincol=min([newcorners1(1,:) newcorners2(1,:)]);
minrow=min([newcorners1(2,:) newcorners2(2,:)]);
T=[1 0 -mincol+1;0 1 -minrow+1;0 0 1]
newcorners1=map_points(T*H1,oldcorners);
newcorners2=map_points(T*H2,oldcorners);
maxcol=max([newcorners1(1,:) newcorners2(1,:)]);
maxrow=max([newcorners1(2,:) newcorners2(2,:)]);
T=inv(diag([maxcol/w maxrow/h 1]))*T

% Apply Transforms

imr1=image_resample(double(im1),T*H1,h,w);
figure(3);imagesc(uint8(imr1));
imr2=image_resample(double(im2),T*H2,h,w);
figure(4);imagesc(uint8(imr2));

new_points1 = map_points(T*H2,y2(1:2,:))
new_points2 = map_points(T*H1,y1(1:2,:))

%% 5 The fundamental matrix and the 8-point algorithm

% Crazy data sets
%y1 = [420 453; 438 285; 86 319; 253 176; 295 100; 275 138; 238 232; 380 383; 397 367; 409 30]';
%y2 = [349 469; 368 307; 93 210; 260 139; 302 81; 284 110; 244 188; 315 381; 440 394; 406 50]';

%y1 = [415 87; 408 30; 439 286; 422 453; 148 340; 238 232; 409 152; 426 31]';
%y2 = [423 113; 405 50; 367 308; 248 468; 145 251; 243 187; 485 188; 432 50]';

%y1 = [ 148 341; 169 380; 182 492; 207 415; 295 103; 275 138; 252 176; 237 233; 433 238; 447 255; 446 270; 440 285; 409 29]';
%y2 = [ 145 252; 162 298; 183 324; 208 350; 301 82; 284 110; 261 139; 243 187; 383 266; 383 283; 377 296; 370 308; 405 50]';

y1 = [ 420 452; 398 367; 439 285; 410 153; 416 86; 409 30; 295 103; 237 232; 94 312; 204 426 ]';
y2 = [ 348 468; 440 394; 370 306; 485 190; 423 111; 406 52; 302 83; 245 189; 101 205; 190 354 ]';

% convert to homogeneous coordinates
y1 = vgg_get_homg(y1);
y2 = vgg_get_homg(y2);

nn8pa
% Display Matrices
F21
F

% Finding epipolar with new F


figure(1);imagesc(im1);
figure(2);imagesc(im2);
l2=F*y1;
l1=F'*y2;
for ix=1:length(y1),
  l1(:,ix)=-l1(:,ix)/norm(l1(1:2,ix))*sign(l1(3,ix)); %Normalise dual
l2(:,ix)=-l2(:,ix)/norm(l2(1:2,ix))*sign(l2(3,ix)); %homog. coord. end for ix=1:size(y1,2),
  figure(1);hold('on');plot(y1(1,ix),y1(2,ix),'or');hold('off');
  figure(1);hold('on');drawline(l1(:,ix));hold('off');
  figure(2);hold('on');plot(y2(1,ix),y2(2,ix),'or');hold('off');
  figure(2);hold('on');drawline(l2(:,ix));hold('off');
end
abs(sum(y1.*l1))
abs(sum(y2.*l2))

% 12.5690   12.0803    0.8660    3.9664    5.9471    3.5920    4.2652    9.4704   14.3562    9.2421
% 12.9365   12.3273    0.8003    3.7042    5.5784    3.3566    3.9824    9.5763   13.2401    8.8655

%% 6 Triangulation

load('tridata','x','y1','y2','im1','im2','C1','C2');

figure(1);imagesc(im1);hold('on');
for ix=1:length(y1),
  plot(y1(2,ix),y1(1,ix),'ro');
end
hold('off');
figure(2);imagesc(im2);hold('on');
for ix=1:length(y2),
  plot(y2(2,ix),y2(1,ix),'ro');
end
hold('off');

xrec=[];
for ix=1:length(y1)
  recpoint = getWCSpoint(C1, C2, y1(:,ix), y2(:,ix)); %INSERT HERE
  xrec=[xrec recpoint];
end

figure(3);plot3(xrec(1,:),xrec(2,:),xrec(3,:),'o');

% Calculate error

error = x - xrec;

mean_of_error = mean(mean(error));
standard_deviation = std2(error);


%%
%Last control (Reprojection error)
error_y1 = [];
error_y2 = [];

for ix=1:length(y2),
new_x = (C1 * xrec(:,ix));
new_y = (C2 * xrec(:,ix)); 
new_x = new_x / new_x(3);
new_y = new_y / new_y(3);


error_y1(ix) = pdist([new_x' ;y1(:,ix)'],'euclidean');
error_y2(ix) = pdist([new_y' ;y2(:,ix)'],'euclidean');
end

mean_error_y1 = mean(error_y1)
mean_error_y2 = mean(error_y2)
standard_deviation_y1 = std2(error_y1)
standard_deviation_y2 = std2(error_y2)
