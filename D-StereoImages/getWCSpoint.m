function xb = getWCSpoint( C1, C2, y1, y2 )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

%y1_mat = ;
%y2_mat = ;

B1 = liu_crossop(y1) * C1;
B2 = liu_crossop(y2) * C2;


B = [B1; B2];

[V D] = eig(B' * B);


diagonal = diag(D);

% Get eigenvector for smallest eigenvalue
[Y I] = sort(diagonal);
x = V(:, I(1));

x = x / x(4);
xb = x;

end

