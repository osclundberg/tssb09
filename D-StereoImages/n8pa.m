% script n8pa
%
% Implements the normalized 8-point algorothm
%
% Assumes that y1 and y2 are 3xN matrices which hold corresponding
% homogeneous image coordinates in their colums, properly normalized so
% that third element = 1.

m1=mean(y1,2);
T1=[eye(2) -m1(1:2);zeros(1,2) 1];
yy1=T1*y1;
d1=mean(sqrt(sum(yy1(1:(2),:).^2)))+eps;
S1=[sqrt(2)*eye(2)/d1 zeros(2,1);zeros(1,2) 1];
T1=S1*T1;
y1n=T1*y1;

m2=mean(y2,2);
T2=[eye(2) -m2(1:2);zeros(1,2) 1];
yy2=T2*y2;
d2=mean(sqrt(sum(yy2(1:(2),:).^2)))+eps;
S2=[sqrt(2)*eye(2)/d2 zeros(2,1);zeros(1,2) 1];
T2=S2*T2;
y2n=T2*y2;

An=[];
for ix=1:size(y1n,2),
  An=[reshape(y2n(:,ix)*y1n(:,ix)',1,9); An];
end

[U S V]=svd(An);
fprintf('Singular values of data matrix:\n');
diag(S)'

Fn_vec=V(:,end);

Fn=reshape(Fn_vec,3,3);

[Uf Sf Vf]=svd(Fn);
S0=Sf;
S0(3,3)=0;
Fn0=Uf*S0*Vf';

F=T2'*Fn0*T1;
