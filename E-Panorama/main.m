
% Setup
addpath('util/')
addpath('images1/')

%% 2 Image Rectification

im = imread('img01.jpg');

% Question 1
% 360 degrees/ 24 images =  15 degrees / image

% Question 2
% Good value for gamma?

gamma = 0.0009; % How do you know a good value for gamma?

dist = image_lensdist_inv('atan', im, gamma);

figure(1); imshow(im);
figure(2); imshow(dist);

% meshgrid

%% 3 Uncalibrated Stitching

% Home exercise: how many correspondence points are needed? Does extra
% points add accuracy?
% Since the honography contains scaling, rotation, and translation
% A minimum of 4 points is needed to solve for all transformations. 
% 1 for rotation (angle), 2 for translation (x,y) 
% and 1 for scaling (s if uniform scaling)
% I suspect increase in points will make no difference
figure
figure(2);

img1 = imread('img01.jpg');
img2 = imread('img02.jpg');
img3 = imread('img03.jpg');
img4 = imread('img04.jpg');

% Image rectification
img_rect1 = image_lensdist_inv('atan', img1, 0.0004);
img_rect2 = image_lensdist_inv('atan', img1, 0.0001);
img_rect3 = image_lensdist_inv('atan', img1, 0.0002);
img_rect4 = image_lensdist_inv('atan', img1, 0.0003);

subplot(2,2,1); imshow(img_rect1);
subplot(2,2,2); imshow(img_rect2);
subplot(2,2,3); imshow(img_rect3);
subplot(2,2,4); imshow(img_rect4);


%%
gamma = 0.0004;

img1 = imread('img01.jpg');
img2 = imread('img02.jpg');
img3 = imread('img03.jpg');
img4 = imread('img04.jpg');
img1 = image_lensdist_inv('atan', img1, gamma);
img2 = image_lensdist_inv('atan', img2, gamma);
img3 = image_lensdist_inv('atan', img3, gamma);
img4 = image_lensdist_inv('atan', img4, gamma);

%[x1,x2]=correspondences_select(img1,img2);
%[x3,x4]=correspondences_select(img2,img3);
%[x5,x6]=correspondences_select(img3,img4);
% save corresp_1to2.mat x1 x2
% save corresp_1to2.mat x1 x2 x3 x4 x5 x6
load('corresp_1to2.mat');

H12 = homography_stls(x1,x2);


% Question: How can you use your function to verify that the homography 
% is correct?

% QUESTION: Does it matter which points you use to estimate the homography? 
% How should they preferably be located in the image?
% Not to close to each other, not in a line, random is the best placement. 

[rows,cols,ndim]=size(img1);
imbox = [1 cols cols 1 1;1 1 rows rows 1];

% QUESTION: Why is it a list of 5 points, do you think? 
% ANS: Origo (?)

imbox12 = map_points(H12,imbox);
figure(1);imshow(img2); hold on
plot(imbox12(1,:),imbox12(2,:),'g')
axis image

%%
% Resample into common registration grid

cl=[imbox imbox12];
cmin=floor(min(cl,[],2));
cmax=ceil(max(cl,[],2));
rows=cmax(2)-cmin(2);
cols=cmax(1)-cmin(1);

Ht=[1 0 1-cmin(1);0 1 1-cmin(2);0 0 1];


pano1=image_resample(img1,Ht*H12,rows,cols);
pano2=image_resample(img2,Ht,rows,cols);


% Generate weight masks
alpha0=ones(size(img1(:,:,1)),'uint8')*255;
alpha1=image_resample(alpha0,Ht*H12,rows,cols);
alpha2=image_resample(alpha0,Ht,rows,cols);

pano=zeros(rows,cols,3,'int32');
asum=int32(alpha1)+int32(alpha2);
asum(asum==0)=1;
for k=1:3,
    pano(:,:,k)=pano(:,:,k)+int32(pano1(:,:,k)).*int32(alpha1);
    pano(:,:,k)=pano(:,:,k)+int32(pano2(:,:,k)).*int32(alpha2);
    pano(:,:,k)=pano(:,:,k)./asum;
end

pano = cast(pano, 'uint8');
imshow(pano);

%% 4 Stitching in Spherical Coordinates

% Kalibration matrix, containing intrinsic camera parameters
K = [1420 -3 808; 0 1420 605; 0 0 1];
[h w dim] = size(img1);
cx = K(1,3);
cy = K(2,3);

hoff = atan( (w/2 - cx)/K(1,1) ); % tan?1((w/2 ? cx)/K11)
voff = atan( (h/2 - cy)/K(2,2) ); % tan?1((h/2 ? cy)/K22)
hfov = 2 * atan( w/2/K(1,1) ); % ?FOV 0.7996 rad, 45.8115 grader 
vfov = 2 * atan( h/2/K(2,2) ); % ?FOV 1.0261 rad, 58.7921 grader


hr=hfov/2*[-1.8 1.8]+ hoff;
vr=vfov/2*[-1.1 1.1]+ voff;

samplingDensity = degtorad(0.05);

% Map x1, x2 .. to spherical coordinates, how?

% 1 -> 2
p1 = map_points(inv(K), x1);
p2 = map_points(inv(K), x2);

% 2 -> 3
p3 = map_points(inv(K), x3);
p4 = map_points(inv(K), x4);


R1 = procrustes(K, p1, p2);
R2 = procrustes(K, p4, p3);


[V D] = eig(R2);

acos(real(D(1,1))) * (180/pi)

uMat = eye(3);

img1_s = image_resample_sphere(img1,K,R1,hr,vr,samplingDensity); 
img2_s = image_resample_sphere(img2,K,uMat,hr,vr,samplingDensity);
img3_s = image_resample_sphere(img3,K,R2,hr,vr,samplingDensity);
imshow(img2_s);

%%
% 
alpha0=ones(size(img1(:,:,1)),'uint8')*255;
alpha1=image_resample_sphere(alpha0,K,R1,hr,vr,samplingDensity);
alpha2=image_resample_sphere(alpha0,K,uMat,hr,vr,samplingDensity);
alpha3=image_resample_sphere(alpha0,K,R2,hr,vr,samplingDensity);

[r, c , dim] = size(alpha1);

pano=zeros(r,c,3,'int32');
asum=int32(alpha1)+int32(alpha2)+int32(alpha3);
asum(asum==0)=1;
for k=1:3,
    pano(:,:,k)=pano(:,:,k)+int32(img1_s(:,:,k)).*int32(alpha1);
    pano(:,:,k)=pano(:,:,k)+int32(img2_s(:,:,k)).*int32(alpha2);
    pano(:,:,k)=pano(:,:,k)+int32(img3_s(:,:,k)).*int32(alpha3);
    pano(:,:,k)=pano(:,:,k)./asum;
end

pano = cast(pano, 'uint8');
figure(1);
imshow(pano);
