#ifndef OpenCV_Matlab_conversion_defined
#define OpenCV_Matlab_conversion_defined

#include "mex.h"
#include "opencv/cv.h"

/* IplImage conversions */
mxArray *copy_IplImage_to_mxArray(const IplImage *ptr_in);
IplImage *copy_mxArray_to_IplImage(const mxArray *ptr_in);

/* CvMat conversions */
CvMat *copy_mxArray_to_CvMat(const mxArray *ptr_in);
mxArray *copy_CvMat_to_mxArray(const CvMat *ptr_in);

/* CvPoint conversions */
mxArray *copy_CvPoint_to_mxArray(const CvPoint *ptr_in, const int N);
mxArray *copy_CvPoint2D32f_to_mxArray(const CvPoint2D32f *ptr_in, const int N);
mxArray *copy_CvPoint2D64f_to_mxArray(const CvPoint2D64f *ptr_in, const int N);
mxArray *copy_CvPoint3D32f_to_mxArray(const CvPoint3D32f *ptr_in, const int N);
mxArray *copy_CvPoint3D64f_to_mxArray(const CvPoint3D64f *ptr_in, const int N);
CvPoint *copy_mxArray_to_CvPoint(const mxArray *ptr_in, int *N);
CvPoint2D32f *copy_mxArray_to_CvPoint2D32f(const mxArray *ptr_in, int *N);
CvPoint2D64f *copy_mxArray_to_CvPoint2D64f(const mxArray *ptr_in, int *N);
CvPoint3D32f *copy_mxArray_to_CvPoint3D32f(const mxArray *ptr_in, int *N);
CvPoint3D64f *copy_mxArray_to_CvPoint3D64f(const mxArray *ptr_in, int *N);

#endif
