#include "mex.h"
#include "opencv/cv.h"
#include "OpenCV_Matlab_conversions.h"
#define MAX_STRING_LENGTH 20

// bjojo 2006-12-20

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
 /* ======================================================================= */
 /* ===== Check the number of input and output arguments              ===== */
 /* ======================================================================= */

  if(nrhs<3 || nrhs>5)
    mexErrMsgTxt("Wrong number of input arguments.");
  if(nlhs>1)
    mexErrMsgTxt("Too many output arguments.");

  /* ======================================================================= */
  /* ===== Convert from Matlab to opencv                               ===== */
  /* ======================================================================= */

  // Source image
  IplImage *src = copy_mxArray_to_IplImage(prhs[0]);
  if (src->depth==IPL_DEPTH_64F)
    mexErrMsgTxt("Input image cannot be of type double.");

  // Map
  IplImage *mapx = copy_mxArray_to_IplImage(prhs[1]);
  IplImage *mapy = copy_mxArray_to_IplImage(prhs[2]);  
  if (mapx->depth!=IPL_DEPTH_32F || mapy->depth!=IPL_DEPTH_32F)
    mexErrMsgTxt("Input map must be 32F (single).");
  if (cvGetDimSize(mapx,0)!=cvGetDimSize(mapy,0) ||
      cvGetDimSize(mapx,1)!=cvGetDimSize(mapy,1))
    mexErrMsgTxt("mapx and mapy must have equal size.");

  // Interpolation method
  char str_code[MAX_STRING_LENGTH];
  int flags=CV_WARP_FILL_OUTLIERS;
  if(nrhs>3 && !mxIsEmpty(prhs[3])){
    mxGetString(prhs[3], str_code, MAX_STRING_LENGTH);
    if(strcmp(str_code,"CV_INTER_NN")==0)
      flags += CV_INTER_NN;
    else if(strcmp(str_code,"CV_INTER_LINEAR")==0)
      flags += CV_INTER_LINEAR;
    else if(strcmp(str_code,"CV_INTER_AREA")==0)
      flags += CV_INTER_AREA;
    else if(strcmp(str_code,"CV_INTER_CUBIC")==0)
      flags += CV_INTER_CUBIC;
    else
      mexErrMsgTxt("Invalid interpolation method.");
  }
  else
    flags += CV_INTER_LINEAR;

  // Fill value
  CvScalar fillval = cvScalar(1);
  if(nrhs>4 && !mxIsEmpty(prhs[4]))
    fillval = cvScalar(mxGetScalar(prhs[4]));
  
  /* ======================================================================= */
  /* ===== Compute                                                     ===== */
  /* ======================================================================= */
  
  // Allocate memory for the destination image
  CvSize dst_size = cvSize( (int)mxGetN(prhs[1]), (int)mxGetM(prhs[1]));  
  IplImage *dst = cvCreateImage( dst_size, src->depth, src->nChannels );

  // Compute
  cvRemap( src, dst, mapx, mapy, flags, fillval);

  /* ======================================================================= */
  /* ===== Convert from opencv to Matlab                               ===== */
  /* ======================================================================= */  

  plhs[0] = copy_IplImage_to_mxArray(dst);

  cvReleaseImage(&src);
  cvReleaseImage(&mapx);
  cvReleaseImage(&mapy);
  cvReleaseImage(&dst);
  return;
}
