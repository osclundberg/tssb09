#include "OpenCV_Matlab_conversions.h"

//----------------------------------------------------------------------
// mxArray stores columns, IplImage and CvMat stores rows, hence the interleave
//
// Some caution has to be made due to the opencv row padding
//----------------------------------------------------------------------

template<class T>
void interleaveTemplate(T* in, T* out, int nx, int ny, int nBands, int dir,int widthstep) {
  int n_xy, m, n, k, i, c = 0;

  // From Matlab to OpenCV
  if (dir == 1) {		/* Matlab order to b0,g0,r0, g1,g1,r1, etc ... */
    unsigned char *outb=(unsigned char*)out;
    n_xy = nx*ny;
    for (m = 0; m < ny; m++) {
      out=(T*)(outb+m*widthstep);
      for (n = 0; n < nx; n++){
        for (k = 0, i = nBands-1; k < nBands; k++, i--) {
          *out = in[m + n*ny + i*n_xy];
          out++;
        }
      }
    }
  }
  // From OpenCV to Matlab
  else {			/* b0,g0,r0, b1,g1,r1, etc ...  to Matlab order */
    unsigned char *inb=(unsigned char*)in;
    for (k = 0, i = nBands-1; k < nBands; k++, i--){
      for (n = 0; n < nx; n++){
        for (m = 0; m < ny; m++) {
          T* inp=(T*)(inb+widthstep*m);
          out[c++]=inp[nBands*n+i];
        }
      }
    }
  }

}

void interleave_CvArr(void* in, void* out, int nx, int ny, int nBands, int dir, int cv_depth, int widthstep){
  switch(cv_depth){
  case CV_8U:
  case CV_8S:
    interleaveTemplate<char>((char*)in, (char*)out, nx, ny, nBands, dir, widthstep);
    break;
    
  case CV_16U:
  case CV_16S:
    interleaveTemplate<short int>((short int*)in, (short int*)out, nx, ny, nBands, dir, widthstep);    
    break;

  case CV_32S:
  case CV_32F:
    interleaveTemplate<int>((int*)in, (int*)out, nx, ny, nBands, dir, widthstep);
    break;
    
  case CV_64F:
    interleaveTemplate<double>((double*)in, (double*)out, nx, ny, nBands, dir, widthstep);
  }
}

//----------------------------------------------------------------------
//----- Class conversion functions                                 -----
//----------------------------------------------------------------------

int mxclass_to_cvtype(const mxArray *mxarr) {

  if (mxIsComplex(mxarr) || mxIsSparse(mxarr))
    mexErrMsgTxt("Input image must be a real matrix");
  
  switch(mxGetClassID(mxarr)){
  case mxLOGICAL_CLASS:
  case mxUINT8_CLASS:
    return CV_8U;
  case mxINT8_CLASS:
    return CV_8S;
  case mxUINT16_CLASS:
    return CV_16U;
  case mxINT16_CLASS:
    return CV_16S;
  case mxINT32_CLASS:
    return CV_32S;
  case mxSINGLE_CLASS:
    return CV_32F;
  case mxDOUBLE_CLASS:
    return CV_64F;
  default:
    mexErrMsgTxt("OPENCV_MEX ERROR: Invalid input data type mxclass_to_cvtype.\n");
    break;
  }
}

mxClassID cvtype_to_mxclass(const int cv_depth) {
  
  switch(cv_depth){
  case CV_8U:
    return mxUINT8_CLASS;
  case CV_8S:
    return mxINT8_CLASS;
  case CV_16U:
    return mxUINT16_CLASS;
  case CV_16S:
    return mxINT16_CLASS;
  case CV_32S:
    return mxINT32_CLASS;
  case CV_32F:
    return mxSINGLE_CLASS;
  case CV_64F:
    return mxDOUBLE_CLASS;
  default:
    mexErrMsgTxt("OPENCV_MEX ERROR: Invalid input data type cvtype_to_mxclass.\n");
    break;
  }
}

//----------------------------------------------------------------------
//----- IplImage conversion functions                              -----
//----------------------------------------------------------------------

IplImage *copy_mxArray_to_IplImage(const mxArray *ptr_in)
{
  int nx, ny, nBands, cv_depth, nDims;
  const int *dim_array;
  IplImage *ptr_out;

  // Get input data facts
  nDims = mxGetNumberOfDimensions(ptr_in);
  dim_array = mxGetDimensions(ptr_in);
  ny = dim_array[0]; nx = dim_array[1];
  if (nDims == 2)
    nBands = 1;
  else
    nBands = dim_array[2];
  cv_depth = mxclass_to_cvtype(ptr_in);

  // Create an iplImage and interleave
  ptr_out = cvCreateImage(cvSize(nx,ny), cvCvToIplDepth(cv_depth), nBands);
  interleave_CvArr(mxGetData(ptr_in), ptr_out->imageData, nx, ny, nBands, 1, cv_depth, ptr_out->widthStep); 

  return ptr_out;
}

mxArray *copy_IplImage_to_mxArray(const IplImage *ptr_in)
{
  int nx, ny, nBands, cv_depth, nDims;
  int dim_array[CV_MAX_DIM];
  mxArray *ptr_out;
  int mxDims[3];

  // Get input data facts
  nDims = cvGetDims(ptr_in,dim_array);
  ny = dim_array[0]; nx = dim_array[1];  
  nBands = ptr_in->nChannels;
  cv_depth = CV_MAT_DEPTH(cvGetElemType(ptr_in));

  // Create mx array dimension array
  mxDims[0] = dim_array[0];
  mxDims[1] = dim_array[1];
  mxDims[2] = nBands;

  // Create an mxarray and interleave
  ptr_out = mxCreateNumericArray(3, mxDims, cvtype_to_mxclass(cv_depth), mxREAL);
  interleave_CvArr(ptr_in->imageData, mxGetData(ptr_out), nx, ny, nBands, 0, cv_depth, ptr_in->widthStep);

  return ptr_out;
}

//----------------------------------------------------------------------
//----- CvMat conversion functions                                 -----
//----------------------------------------------------------------------

CvMat *copy_mxArray_to_CvMat(const mxArray *ptr_in)
{
  int nx, ny, nBands, cv_depth, nDims;
  const int *dim_array;
  CvMat *ptr_out;

  // Get input data facts
  nDims = mxGetNumberOfDimensions(ptr_in);
  dim_array = mxGetDimensions(ptr_in);
  ny = dim_array[0]; nx = dim_array[1];
  if (nDims!=2)
    mexErrMsgTxt("mxArray must be 2D.\n");
  cv_depth = mxclass_to_cvtype(ptr_in);

  // Create a CvMat and interleave
  ptr_out = cvCreateMat(ny, nx, cv_depth);
  interleave_CvArr(mxGetData(ptr_in), ptr_out->data.ptr, nx, ny, 1, 1, cv_depth, ptr_out->step); 

  return ptr_out;
}

mxArray *copy_CvMat_to_mxArray(const CvMat *ptr_in)
{
  int nx, ny, nBands, cv_depth, nDims;
  int dim_array[CV_MAX_DIM];
  mxArray *ptr_out;
  int mxDims[2];

  // Get input data facts
  nDims = cvGetDims(ptr_in,dim_array);
  ny = dim_array[0]; nx = dim_array[1];
  cv_depth = CV_MAT_DEPTH(cvGetElemType(ptr_in));  

  // Create mx array dimension array
  mxDims[0] = dim_array[0];
  mxDims[1] = dim_array[1];

  // Create an mxarray and interleave
  ptr_out = mxCreateNumericArray(2, mxDims, cvtype_to_mxclass(cv_depth), mxREAL);
  interleave_CvArr(ptr_in->data.ptr, mxGetData(ptr_out), nx, ny, 1, 0, cv_depth, ptr_in->step);

  return ptr_out;
}

//----------------------------------------------------------------------
//----- CvPoint conversion functions                               -----
//----------------------------------------------------------------------

// From opencv to Matlab
//-----------------------

mxArray *copy_CvPoint_to_mxArray(const CvPoint *ptr_in, const int N)
{
  int i, mxDims[2];
  mxArray *ptr_out;
  int *tmp;

  // Create an array
  mxDims[0] = 2;
  mxDims[1] = N;
  ptr_out = mxCreateNumericArray(2, mxDims, mxINT8_CLASS, mxREAL);  
  tmp = (int *)mxGetPr(ptr_out);
  
  // Copy data
  for (i=0;i<N;i++){
    tmp[2*i] = ptr_in[i].x;
    tmp[2*i+1] = ptr_in[i].y;
  }  
  return ptr_out;
}

mxArray *copy_CvPoint2D32f_to_mxArray(const CvPoint2D32f *ptr_in, const int N)
{
  int i, mxDims[2];
  mxArray *ptr_out;
  float *tmp;

  // Create an array
  mxDims[0] = 2;
  mxDims[1] = N;
  ptr_out = mxCreateNumericArray(2, mxDims, mxSINGLE_CLASS, mxREAL);
  tmp = (float *)mxGetPr(ptr_out);
  
  // Copy data
  for (i=0;i<N;i++){
    tmp[2*i] = ptr_in[i].x;
    tmp[2*i+1] = ptr_in[i].y;
  }  
  return ptr_out;
}

mxArray *copy_CvPoint2D64f_to_mxArray(const CvPoint2D64f *ptr_in, const int N)
{
  int i, mxDims[2];
  mxArray *ptr_out;
  double *tmp;

  // Create an array
  mxDims[0] = 2;
  mxDims[1] = N;
  ptr_out = mxCreateNumericArray(2, mxDims, mxDOUBLE_CLASS, mxREAL);  
  tmp = (double *)mxGetPr(ptr_out);
  
  // Copy data
  for (i=0;i<N;i++){
    tmp[2*i] = ptr_in[i].x;
    tmp[2*i+1] = ptr_in[i].y;
  }  
  return ptr_out;
}

mxArray *copy_CvPoint3D32f_to_mxArray(const CvPoint3D32f *ptr_in, const int N)
{
  int i, mxDims[2];
  mxArray *ptr_out;
  float *tmp;

  // Create an array
  mxDims[0] = 3;
  mxDims[1] = N;
  ptr_out = mxCreateNumericArray(2, mxDims, mxSINGLE_CLASS, mxREAL);  
  tmp = (float *)mxGetPr(ptr_out);
  
  // Copy data
  for (i=0;i<N;i++){
    tmp[2*i] = ptr_in[i].x;
    tmp[2*i+1] = ptr_in[i].y;
    tmp[2*i+2] = ptr_in[i].z;
  }  
  return ptr_out;
}

mxArray *copy_CvPoint3D64f_to_mxArray(const CvPoint3D64f *ptr_in, const int N)
{
  int i, mxDims[2];
  mxArray *ptr_out;
  double *tmp;

  // Create an array
  mxDims[0] = 3;
  mxDims[1] = N;
  ptr_out = mxCreateNumericArray(2, mxDims, mxDOUBLE_CLASS, mxREAL);  
  tmp = (double *)mxGetPr(ptr_out);
  
  // Copy data
  for (i=0;i<N;i++){
    tmp[2*i] = ptr_in[i].x;
    tmp[2*i+1] = ptr_in[i].y;
    tmp[2*i+2] = ptr_in[i].z;
  }  
  return ptr_out;
}

// From Matlab to opencv
//-----------------------

CvPoint *copy_mxArray_to_CvPoint(const mxArray *ptr_in, int *N)
{
  int i;
  CvPoint *ptr_out;
  int *tmp;
  
  // Create a point list
  if (mxGetM(ptr_in)!=2)
    mexErrMsgTxt("mxArray must be 2xN.");
  *N = mxGetN(ptr_in);
  ptr_out = (CvPoint*)cvAlloc(*N*sizeof(CvPoint));

  // Copy data
  tmp = (int *)mxGetPr(ptr_in);
  for (i=0;i<*N;i++){
    ptr_out[i].x = tmp[2*i];
    ptr_out[i].y = tmp[2*i+1];
  }
  return ptr_out;
}

CvPoint2D32f *copy_mxArray_to_CvPoint2D32f(const mxArray *ptr_in, int *N)
{
  int i;
  CvPoint2D32f *ptr_out;
  float *tmp;
  
  // Create a point list
  if (mxGetM(ptr_in)!=2)
    mexErrMsgTxt("mxArray must be 2xN.");
  *N = mxGetN(ptr_in);
  ptr_out = (CvPoint2D32f*)cvAlloc(*N*sizeof(CvPoint2D32f));

  // Copy data
  tmp = (float *)mxGetPr(ptr_in);  
  for (i=0;i<*N;i++){
    ptr_out[i].x = tmp[2*i];
    ptr_out[i].y = tmp[2*i+1];
  }
  return ptr_out;
}

CvPoint2D64f *copy_mxArray_to_CvPoint2D64f(const mxArray *ptr_in, int *N)
{
  int i;
  CvPoint2D64f *ptr_out;
  double *tmp;
  
  // Create a point list
  if (mxGetM(ptr_in)!=2)
    mexErrMsgTxt("mxArray must be 2xN.");
  *N = mxGetN(ptr_in);
  ptr_out = (CvPoint2D64f*)cvAlloc(*N*sizeof(CvPoint2D64f));

  // Copy data
  tmp = (double *)mxGetPr(ptr_in);  
  for (i=0;i<*N;i++){
    ptr_out[i].x = tmp[2*i];
    ptr_out[i].y = tmp[2*i+1];
  }
  return ptr_out;
}

CvPoint3D32f *copy_mxArray_to_CvPoint3D32f(const mxArray *ptr_in, int *N)
{
  int i;
  CvPoint3D32f *ptr_out;
  float *tmp;
  
  // Create a point list
  if (mxGetM(ptr_in)!=3)
    mexErrMsgTxt("mxArray must be 2xN.");
  *N = mxGetN(ptr_in);
  ptr_out = (CvPoint3D32f*)cvAlloc(*N*sizeof(CvPoint3D32f));

  // Copy data
  tmp = (float *)mxGetPr(ptr_in);  
  for (i=0;i<*N;i++){
    ptr_out[i].x = tmp[2*i];
    ptr_out[i].y = tmp[2*i+1];
    ptr_out[i].z = tmp[2*i+2];
  }
  return ptr_out;
}

CvPoint3D64f *copy_mxArray_to_CvPoint3D64f(const mxArray *ptr_in, int *N)
{
  int i;
  CvPoint3D64f *ptr_out;
  double *tmp;
  
  // Create a point list
  if (mxGetM(ptr_in)!=3)
    mexErrMsgTxt("mxArray must be 2xN.");
  *N = mxGetN(ptr_in);
  ptr_out = (CvPoint3D64f*)cvAlloc(*N*sizeof(CvPoint3D64f));

  // Copy data
  tmp = (double *)mxGetPr(ptr_in);  
  for (i=0;i<*N;i++){
    ptr_out[i].x = tmp[2*i];
    ptr_out[i].y = tmp[2*i+1];
    ptr_out[i].z = tmp[2*i+2];
  }
  return ptr_out;
}
