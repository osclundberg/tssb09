function dst = cvRemap_mex(src,mapx,mapy,method,fillval)

% dst = cvRemap_mex(src,mapx,mapy,method,fillval)
%
% Applies generic geometrical transformation to an image.
% NB! Note that the maps, mapx and mapy, use the C-array
% convention, i.e. the upper left pixel has coordinates (0,0).
%
% For more help on the parameters and their default values, 
% see the opencv documentation.
% Except for input string method, which can be one of
%       'CV_INTER_NN'
%       'CV_INTER_LINEAR' (default if nonexisting or empty)
%       'CV_INTER_AREA'
%       'CV_INTER_CUBIC'
% (The flags variable is set to method+CV_WARP_FILL_OUTLIERS)
%
% BUGS: - It seems that the fill value (fillval) only affects the
%         blue channel if src is a color RGB image. The other
%         channels are set to zero.
%       - It also seems that the methods 'CV_INTER_NN',
%         'CV_INTER_LINEAR', and 'CV_INTER_AREA' give the same
%         result.

% bjojo 2006-12-20

error(['cvRemap is implemented as a mex-file. It does not seem to be available on this platform.'])
