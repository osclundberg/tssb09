function x2 = map_points( H, x1 )
%UNTITLED3 Summary of this function goes here
%   mapping points in x1 to x2 using homography matrix H.
%   x1 is a 3xN matrix with homogeneous coordinates in image 1
%   x2 is above coordinates mapped to image 2.

x1 = [x1;ones(1,size(x1,2))];

x2 = H * x1;


x2(1, :) = x2(1, :) ./ x2(3, :); 
x2(2, :) = x2(2, :) ./ x2(3, :); 

x2 = x2(1:2,:);

end

