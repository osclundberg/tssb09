function R = procrustes( K, p1, p2 )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here



% Transform to homogeneous coordinates.
P1 = [p1;ones(1,size(p1,2))];
P2 = [p2;ones(1,size(p2,2))];

divval = sqrt(sum(p1.^2, 1));

for k = 1:3
    P1(k,:) = P1(k,:) ./ divval; 
end

divval = sqrt(sum(p2.^2, 1));

for k = 1:3
    P2(k,:) = P2(k,:) ./ divval; 
end

[U,D,V]=svd(P2*P1');

R = U * V';

end

